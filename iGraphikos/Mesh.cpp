#include "Mesh.h"

#include <gl\GL.h>
#include <gl\GLU.h>
#include <fstream>

Mesh::Mesh()
{
	Reset();
}

Mesh::~Mesh()
{
	Reset();
}

Int32 Mesh::OpenFile( std::string& filename )
{
	Reset();

	std::ifstream objfile;
	objfile.open( filename );

	if( !objfile.is_open() )
		return _ERR;

	Index a, b, c, d;
	Vector3 pos, norm;
	Vector2 tex;

	char buffer[256];
	while( !objfile.eof() )
	{
		objfile.getline( buffer, 256 );

		if( buffer[0] == '#' )
			continue;
		else if( buffer[0] == 'v' )
		{
			if( buffer[1] == ' ' )
			{
				sscanf_s( buffer, "v %f %f %f", &pos.x, &pos.y, &pos.z );

				if( m_pos.size() == 0 )
				{
					m_min = pos;
					m_max = pos;
				}
				else
				{
					if( pos.x < m_min.x ) m_min.x = pos.x;
					if( pos.y < m_min.y ) m_min.y = pos.y;
					if( pos.z < m_min.z ) m_min.z = pos.z;
					if( pos.x > m_max.x ) m_max.x = pos.x;
					if( pos.y > m_max.y ) m_max.y = pos.y;
					if( pos.z > m_max.z ) m_max.z = pos.z;
				}

				m_pos.push_back( pos );
			}
			else if( buffer[1] == 't' )
			{
				sscanf_s( buffer, "vt %f %f", &tex.x, &tex.y );
				m_tex.push_back( tex );
			}
			else if( buffer[1] == 'n' )
			{
				sscanf_s( buffer, "vn %f %f %f", &norm.x, &norm.y, &norm.z );
				m_norm.push_back( norm );
			}
		}
		else if( buffer[0] == 'f' )
		{
			int nSlash = 0;
			int nCount = 0;
			int bEnable = 0;
			for( int i = 0; buffer[i] != '\0'; i++ )
			{
				if( buffer[i] != ' ' )
				{
					if( bEnable )
					{
						nCount++;
						bEnable = 0;
					}
				}
				else
					bEnable = 1;

				if( buffer[i] == '/' )
					nSlash++;
			}

			if( nCount == 4 )
			{
				if( m_tex.size() > 0 && m_norm.size() > 0 )
				{
					sscanf_s( buffer, "f %d/%d/%d %d/%d/%d %d/%d/%d %d/%d/%d", 
						&a.v, &a.t, &a.n, &b.v, &b.t, &b.n, &c.v, &c.t, &c.n, &d.v, &d.t, &d.n );
				}
				else if( m_norm.size() > 0 )
				{
					if( nSlash > 4 )
						sscanf_s( buffer, "f %d//%d %d//%d %d//%d %d//%d", &a.v, &a.n, &b.v, &b.n, &c.v, &c.n, &d.v, &d.n );
					else
						sscanf_s( buffer, "f %d/%d %d/%d %d/%d %d/%d", &a.v, &a.n, &b.v, &b.n, &c.v, &c.n, &d.v, &d.n );
					a.t = b.t = c.t = d.t = 0;
				}
				else if( m_tex.size() > 0 )
				{
					if( nSlash > 4 )
						sscanf_s( buffer, "f %d//%d %d//%d %d//%d %d//%d", &a.v, &a.t, &b.v, &b.t, &c.v, &c.t, &d.v, &d.t );
					else
						sscanf_s( buffer, "f %d/%d %d/%d %d/%d %d/%d", &a.v, &a.t, &b.v, &b.t, &c.v, &c.t, &d.v, &d.t );
					a.n = b.n = c.n = d.n = 0;
				}
				else
				{
					sscanf_s( buffer, "f %d %d %d %d", &a.v, &b.v, &c.v, &d.v );
					a.t = b.t = c.t = d.t = 0;
					a.n = b.n = c.n = d.n = 0;
				}
				a.v--; b.v--; c.v--; d.v--;
				a.t--; b.t--; c.t--; d.t--;
				a.n--; b.n--; c.n--; d.n--;

				m_quad.push_back( a );
				m_quad.push_back( b );
				m_quad.push_back( c );
				m_quad.push_back( d );
			}
			else if( nCount == 3 )
			{
				if( m_tex.size() > 0 && m_norm.size() > 0 )
				{
					sscanf_s( buffer, "f %d/%d/%d %d/%d/%d %d/%d/%d", 
						&a.v, &a.t, &a.n, &b.v, &b.t, &b.n, &c.v, &c.t, &c.n );
				}
				else if( m_norm.size() > 0 )
				{
					sscanf_s( buffer, "f %d/%d %d/%d %d/%d", &a.v, &a.n, &b.v, &b.n, &c.v, &c.n );
					a.t = b.t = c.t = 0;
				}
				else if( m_tex.size() > 0 )
				{
					sscanf_s( buffer, "f %d/%d %d/%d %d/%d", &a.v, &a.t, &b.v, &b.t, &c.v, &c.t );
					a.n = b.n = c.n = 0;
				}
				else
				{
					sscanf_s( buffer, "f %d %d %d", &a.v, &b.v, &c.v );
					a.t = b.t = c.t = 0;
					a.n = b.n = c.n = 0;
				}
				a.v--; b.v--; c.v--;
				a.t--; b.t--; c.t--;
				a.n--; b.n--; c.n--;

				m_tri.push_back( a );
				m_tri.push_back( b );
				m_tri.push_back( c );
			}
		}
	}
	objfile.close();

	if( m_norm.size() == 0 )
		_CalcNormals();

	_FixSize();

	return _OK;
}

Int32 Mesh::Render( MCData* pMCData )
{
	glEnable( GL_LIGHTING );
	glEnable( GL_LIGHT0 );

	if( m_culling )
	{
		glEnable( GL_CULL_FACE );
		glCullFace( m_culling == 1 ? GL_BACK : GL_FRONT );
	}
	else
		glDisable( GL_CULL_FACE );

	glPolygonMode( GL_FRONT_AND_BACK, m_wireframe ? GL_LINE : GL_FILL );

	if( pMCData )
	{
		if( pMCData->size() < m_pos.size() )
		{
			pMCData->reserve( m_pos.size() );
			pMCData->resize( m_pos.size() );
		}
	}

	if( m_tri.size() > 0 )
	{
		glBegin( GL_TRIANGLES );
		for( Int32 i = 0; i < m_tri.size(); i++ )
			_RenderIndex( m_tri[i], pMCData );
		glEnd();
	}

	if( m_quad.size() > 0 )
	{
		Int32 c = m_tri.size();
		glBegin( GL_QUADS );
		for( Int32 i = 0; i < m_quad.size(); i++ )
			_RenderIndex( m_quad[i], pMCData );
		glEnd();
	}

	glDisable( GL_LIGHT0 );
	glDisable( GL_LIGHTING );

	return _OK;
}

void Mesh::Reset()
{
	m_culling = 0;
	m_wireframe = 0;
	m_invnorm = 0;
	m_pos.clear();
	m_tex.clear();
	m_norm.clear();
	m_tri.clear();
	m_quad.clear();
	m_min.Zero();
	m_max.Zero();
}

void Mesh::_RenderIndex( const Index& i, MCData* pMCData )
{
	if( i.t >= 0 )
		glTexCoord2f( m_tex[i.t].x, m_tex[i.t].y );

	if( i.n >= 0 )
	{
		if( m_invnorm )
			glNormal3f( -m_norm[i.n].x, -m_norm[i.n].y, -m_norm[i.n].z );
		else
			glNormal3f( m_norm[i.n].x, m_norm[i.n].y, m_norm[i.n].z );
	}

	if( pMCData )
		glColor3f( (*pMCData)[i.v].x, (*pMCData)[i.v].y, (*pMCData)[i.v].z );

	glVertex3f( m_pos[i.v].x, m_pos[i.v].y, m_pos[i.v].z );
}


void Mesh::_CalcNormals()
{
	if( m_pos.size() == 0 ) 
		return;

	Vector3 v1, v2, norm = Vector3( 0, 0, 0 );
	m_norm.reserve( m_pos.size() );
	for( Int32 i = 0; i < m_pos.size(); i++ )
		m_norm.push_back( norm );

	for( int i = 0; i < m_tri.size() / 3; i++ )
	{
		Index& a = m_tri[i*3];
		Index& b = m_tri[i*3+1];
		Index& c = m_tri[i*3+2];

		a.n = a.v;
		b.n = b.v;
		c.n = c.v;

		v1 = m_pos[b.v] - m_pos[a.v];
		v2 = m_pos[c.v] - m_pos[a.v];

		norm = Vector3::Cross( v1, v2 ).GetNormal();

		m_norm[a.v] += norm;
		m_norm[b.v] += norm;
		m_norm[c.v] += norm;
	}

	for( int i = 0; i < m_quad.size() / 4; i++ )
	{
		Index& a = m_quad[i*4];
		Index& b = m_quad[i*4+1];
		Index& c = m_quad[i*4+2];
		Index& d = m_quad[i*4+3];

		a.n = a.v;
		b.n = b.v;
		c.n = c.v;
		d.n = d.v;

		v1 = m_pos[b.v] - m_pos[a.v];
		v2 = m_pos[c.v] - m_pos[a.v];

		norm = Vector3::Cross( v1, v2 ).GetNormal();

		m_norm[a.v] += norm;
		m_norm[b.v] += norm;
		m_norm[c.v] += norm;
		m_norm[d.v] += norm;
	}

	for( Int32 i = 0; i < m_norm.size(); i++ )
		m_norm[i].Normalize();
}

void Mesh::_FixSize()
{
	Vector3 diff = m_max - m_min;
	Vector3 move = m_min + diff * 0.5f;

	Real scale;
	if( diff.x > diff.y )
		scale = 1.0f / ( diff.x > diff.z ? diff.x : diff.z );
	else
		scale = 1.0f / ( diff.y > diff.z ? diff.y : diff.z );

	for( Int32 i = 0; i < m_pos.size(); i++ )
	{
		m_pos[i] -= move;
		m_pos[i] *= scale;
	}

	m_moved = move;
	m_scaled = scale;
}

Int32 Mesh::GetPoly( Int32 i, Index poly[4] ) const
{
	if( i < m_tri.size()/3 )
	{
		i *= 3;
		poly[0] = m_tri[i];
		poly[1] = m_tri[i+1];
		poly[2] = m_tri[i+2];
		return 3;
	}
	else
	{
		i -= m_tri.size()/3;
		i *= 4;
		poly[0] = m_quad[i];
		poly[1] = m_quad[i+1];
		poly[2] = m_quad[i+2];
		poly[3] = m_quad[i+3];
		return 4;
	}
}

Int32 Mesh::ToggleCullSide()
{
	m_culling++;
	if( m_culling == 3 )
		m_culling = 0;
	return m_culling;
}

Int32 Mesh::ToggleWireframe()
{
	m_wireframe = !m_wireframe;
	return m_wireframe;
}

Int32 Mesh::ToggleInvNorm()
{
	m_invnorm = !m_invnorm;
	return m_invnorm;
}
