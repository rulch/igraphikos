
// just contains basic vector/matrix math

#ifndef __SE_LINEARMATH__
#define __SE_LINEARMATH__

#define SE_MATRIX_COLMAJOR

#include <stdio.h>

#define DEBUGOUT( str, ... ) { char out[256]; sprintf( out, str, ##__VA_ARGS__ ); printf("%s", out ); }
#include <stdlib.h>
#define _ERR -1
#define _OK 0
#define Failed( x ) ( x < 0 )
// End

#include "SEStdDataTypes.h"
#include "SEStdMemory.h"
#include "SEStdMath.h"

#ifdef __USE_D3DX__
#include <D3DX10math.h>
#endif

typedef UInt16 VIndex;

struct Vector2;
struct Vector3;
struct Vector4;

struct IVector2;
struct IVector3;
struct IVector4;

struct Quaternion;

struct Matrix3x3;
struct Matrix4x4;
struct Matrix3x4;

#include "SEVector.h"

#include "SEMatrix.h"


#endif
