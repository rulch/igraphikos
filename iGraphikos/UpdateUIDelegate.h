//
//  UpdateUIDelegate.h
//  iGraphikos
//
//  Created by Ryan E. Ulch on 11/28/13.
//  Copyright (c) 2013 Ryan E. Ulch. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UpdateUIDelegate <NSObject>

-(void)UpdateAnimationSlider:(float)value;
-(void)SetMeshStringsRef:(NSArray*)aryMeshStrings;

@end
