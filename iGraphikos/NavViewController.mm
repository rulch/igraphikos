//
//  NavViewController.m
//  iGraphikos
//
//  Created by Ryan E. Ulch on 11/24/13.
//  Copyright (c) 2013 Ryan E. Ulch. All rights reserved.
//

#import "NavViewController.h"
#import "ViewController.h"

@interface NavViewController ()
{
    DemoEnum_t          _demoType;
}
@end

@implementation NavViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _demoType = DEMO_CATMULL_CLARK;
    _levelStepper.value = 0;
    
    // Set title for navigation view
    UIButton* button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.userInteractionEnabled = NO;
    [button setTitle:@"iGraphikos" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:22];
    
    self.navigationItem.titleView = button;

    _mLaplacianInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"laplacianInfoController"];
    _mLaplacianInfoController.demoType = DEMO_LAPLACIAN;
    
    _mCatmullInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"catmullInfoController"];
    _mCatmullInfoController.demoType = DEMO_CATMULL_CLARK;
    
    _mLoopInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"loopInfoController"];
    _mLoopInfoController.demoType = DEMO_LOOP;
    
    _mHalfEdgeInfoController = [self.storyboard instantiateViewControllerWithIdentifier:@"halfEdgeInfoController"];
    _mHalfEdgeInfoController.demoType = DEMO_NONE;
    
    _mInformationViewController = _mCatmullInfoController;
    _mInformationViewController.demoType = DEMO_CATMULL_CLARK;
    
    _mCatmullInfoController.mHalfEdgeInfo = _mHalfEdgeInfoController;
    _mLoopInfoController.mHalfEdgeInfo = _mHalfEdgeInfoController;
    _mLaplacianInfoController.mHalfEdgeInfo = _mHalfEdgeInfoController;
    
    
    [self SetFreshUIState];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ShowDemoChoices:(UIButton *)sender
{
    NSString *actionSheetTitle = @"Choose Demo"; //Action Sheet Title
    NSString *other1 = @"Catmull-Clark Subdivision";
    NSString *other2 = @"Loop Subdivision";
    NSString *other3 = @"Laplacian";
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2, other3, nil];
    
    CGRect pos = [sender frame];
    [actionSheet showFromRect:pos inView:self.view animated:TRUE];
}

- (IBAction)ShowMeshChoices:(UIButton *)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose Mesh"
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    // ObjC Fast Enumeration
    for (NSString *title in _meshStrings) {
        [actionSheet addButtonWithTitle:title];
    }
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Cancel"];
    
    CGRect pos = [sender frame];
    [actionSheet showFromRect:pos inView:self.view animated:TRUE];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if( buttonIndex == [actionSheet cancelButtonIndex])
        return;
    
    NSString* option;
    if( [actionSheet.title isEqualToString: @"Choose Demo"])
    {
        _demoType = DEMO_NONE;
        option = [actionSheet buttonTitleAtIndex:buttonIndex];
        if( [option isEqualToString:@"Catmull-Clark Subdivision"])
        {
            _demoType = DEMO_CATMULL_CLARK;
            _mInformationViewController = _mCatmullInfoController;
            _levelStepper.autorepeat = NO;
        }
        else if( [option isEqualToString:@"Loop Subdivision"] )
        {
            _demoType = DEMO_LOOP;
            _mInformationViewController = _mLoopInfoController;
            _levelStepper.autorepeat = NO;
        }
        else if( [option isEqualToString:@"Laplacian"] )
        {
            _mInformationViewController = _mLaplacianInfoController;
            _demoType = DEMO_LAPLACIAN;
            _levelStepper.autorepeat = YES;
        }
        else //somehow, invalid option encountered
        {
            NSLog(@"Invalid demo option encountered in actionSheet:didDismissWithButtonIndex");
            return;
        }

        [[self communicationDelegate] SwitchDemo: _demoType];
    }
    else if( [actionSheet.title isEqualToString: @"Choose Mesh"] )
    {
        MeshEnum_t meshType = MESH_NONE;
        option = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        for( int i = 0; i < [_meshStrings count]; ++i )
        {
            NSString* curr = [_meshStrings objectAtIndex:i];
            if( [curr isEqualToString:option] )
            {
                meshType = (MeshEnum_t)i;
                [_meshButton setTitle:curr forState:UIControlStateNormal];
                [[self communicationDelegate] SwitchMesh: meshType];
                break;
            }
        }
    }
    [self SetFreshUIState];
}

- (IBAction)TriggerHelpMenu:(id)sender
{
    UIAlertView* helpView = [[UIAlertView alloc]
                             initWithTitle:@"Controls"
                             message:@"Rotate camera with a single finger\n\nAnimate by dragging two fingers horizontally\n\nRandomly modify the current mesh by shaking the iPad!"
                             delegate:self
                             cancelButtonTitle:@"Okay"
                             otherButtonTitles: nil];
    [helpView show];

}

- (IBAction)ToggleShowSolid:(UISwitch *)sender
{
    [_communicationDelegate SetShowSolid: sender.on];
}

- (IBAction)ToggleBorderHandling:(UISwitch *)sender
{
    [_communicationDelegate SetBorderHandling:sender.on];
}

- (IBAction)ToggleShowPatchColors:(UISwitch *)sender
{
    [_communicationDelegate SetShowPatchColors:sender.on];
}

- (IBAction)ToggleEdgeMode:(UISwitch *)sender
{
    [_communicationDelegate SetPickingMode:sender.on];
    
    _animationSlider.value = 1.0f;
    [self HandleAnimationController:_animationSlider];
    
    if( sender.on )
    {
        _animationSlider.userInteractionEnabled = NO;
        _animationSlider.alpha = 0.25;
        
        _levelStepper.userInteractionEnabled = NO;
        _levelStepper.alpha = 0.25;
        
        _wireframeSwitch.userInteractionEnabled = NO;
        _wireframeSwitch.alpha = 0.25;
        
        if( !_wireframeSwitch.on )
        {
            _wireframeSwitch.on = YES;
            [_communicationDelegate SetWireFrame:YES];
        }
    }
    else
    {
        _animationSlider.userInteractionEnabled = YES;
        _animationSlider.alpha = 1.0;
        
        _levelStepper.userInteractionEnabled = YES;
        _levelStepper.alpha = 1.0;
        
        _wireframeSwitch.userInteractionEnabled = YES;
        _wireframeSwitch.alpha = 1.0;
    }
    
}


- (IBAction)HandleLevelStepper:(UIStepper *)sender
{
    NSString* value = [NSString stringWithFormat:@"%.02f", (float)sender.value];
    [_levelCounterLabel setText: value ];

    // Disabling interactions during processing
    _levelStepper.userInteractionEnabled = NO;
    
    // Laplacian completes too fast to require the animation
    NSThread *thread = [[NSThread alloc]initWithTarget:self selector:@selector(ShowProgress) object:nil];
    if( _demoType != DEMO_LAPLACIAN )
    {
        [thread start];
    }
    
    [_communicationDelegate SetLevel:sender.value];
    
    _levelStepper.userInteractionEnabled = YES;
    
    if( _demoType == DEMO_LAPLACIAN )
    {
        sender.minimumValue = sender.value;
    }
    
    if( _levelStepper.value > 0)
    {
        if( _levelStepper.value == 1 || _demoType != DEMO_LAPLACIAN )
            _animationSlider.value = 0;
        
        _animationSlider.userInteractionEnabled = YES;
        _animationSlider.alpha = 1.0;

    }
    else
    {
        _animationSlider.value = 1;
        _animationSlider.userInteractionEnabled = NO;
        _animationSlider.alpha = 0.25;
    }
    
    [_communicationDelegate SetAnimationConstant:_animationSlider.value];
    [self SetUIState];
    while( [thread isExecuting ] ) {}
    [_levelProgressIndicator stopAnimating];
}

- (IBAction)HandleAnimationController:(UISlider *)sender
{
    [_communicationDelegate SetAnimationConstant:sender.value];
    [self SetUIState];
}

- (IBAction)ToggleWireFrame:(UISwitch *)sender
{
    [_communicationDelegate SetWireFrame:sender.on];
}

-(void)UpdateAnimationSlider:(float)value
{
    _animationSlider.value = value;
}

-(void)SetMeshStringsRef:(NSArray*)aryMeshStrings
{
    _meshStrings = aryMeshStrings;
}
-(void)ShowProgress
{
    [_levelProgressIndicator startAnimating];
}

-(void)SetUIState
{
    float value;
    
    if( _demoType == DEMO_CATMULL_CLARK || _demoType == DEMO_LOOP )
        value = _animationSlider.value + _levelStepper.value - 1;
    else if( _demoType == DEMO_LAPLACIAN )
        value = _animationSlider.value * _levelStepper.value;
    else
        return;
    
    NSString* valString = [NSString stringWithFormat:@"%.02f", value];
    [_levelCounterLabel setText: valString ];
}

-(void)SetFreshUIState
{
    _levelStepper.minimumValue = 0;
    _levelStepper.value = 0;
    _animationSlider.value = 1;
    
    if( _demoType == DEMO_LOOP || _demoType == DEMO_CATMULL_CLARK )
    {
        _borderLabel.hidden = NO;
        _borderSwitch.hidden = NO;
        _borderSwitch.on = YES;
        
        _edgeLabel.hidden = NO;
        _edgeModeSwitch.hidden = NO;
        _edgeModeSwitch.on = NO;
    }
    else
    {
        _borderLabel.hidden = YES;
        _borderSwitch.hidden = YES;
        
        _edgeLabel.hidden = YES;
        _edgeModeSwitch.hidden = YES;
        _edgeModeSwitch.on = NO;
    }
    
    if( _demoType == DEMO_CATMULL_CLARK )
    {
        _levelStepper.maximumValue = 5;
        [_demoButton setTitle:@"Catmull-Clark" forState:UIControlStateNormal];
    }

        
    if( _demoType == DEMO_LOOP )
    {
        _levelStepper.maximumValue = 5;
        [_demoButton setTitle:@"Loop" forState:UIControlStateNormal];
        
    }
    else if( _demoType == DEMO_LAPLACIAN )
    {
        _levelStepper.maximumValue = 100;
        [_demoButton setTitle:@"Laplacian" forState:UIControlStateNormal];
    }
    _animationSlider.userInteractionEnabled = NO;
    _animationSlider.alpha = 0.25;
    
    _mShowPatchColors.on = NO;
    _wireframeSwitch.userInteractionEnabled = YES;
    _wireframeSwitch.alpha = 1.0f;
    
    [_communicationDelegate SetShowPatchColors:NO];
    [_communicationDelegate SetPickingMode:NO];
    
    [_communicationDelegate SetAnimationConstant:_animationSlider.value];
    [self SetUIState];
}


- (IBAction)ShowMoreInformation:(id)sender {
    _mInformationViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    _mInformationViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;

    [self presentViewController:_mInformationViewController animated:NO completion:nil];
}

- (IBAction)ShowSourceCodeLink:(UIButton *)sender
{
    [[UIApplication sharedApplication] openURL:
                [NSURL URLWithString:@"https://bitbucket.org/rulch/igraphikos"]];
}


-(CGRect)GetFrameForCurrentOrientation:(UIInterfaceOrientation) orientation
{
    UIScreen* screen = [UIScreen mainScreen];
    CGRect screenBounds = screen.bounds;
    
    if( UIInterfaceOrientationIsLandscape(orientation) )
    {
        CGFloat defaultWidth = 500.0f;
        screenBounds = CGRectMake(0, 0, defaultWidth, 0);
    }
    
    return screenBounds;
}
@end























































