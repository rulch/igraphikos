#ifndef __COLORS__
#define __COLORS__

#include "SELinearMath.h"

unsigned int GetUniqueColor( int i );
Vector4 GetUniqueColor4f( int i );

#endif
