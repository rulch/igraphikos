#ifndef __HEDATA__
#define __HEDATA__

#include "Mesh.h"

struct HalfEdge
{
	Int32 v, f, pair, next, hard;
};

class HEData
{
private:
	const Mesh*				m_pMesh;
	std::vector<HalfEdge>	m_he;
    Int32                   m_bordercount;

	Int32 FirstNeighbor( Int32 h );
	Int32 NextNeighbor( Int32 h );

public:
	const HalfEdge& operator[]( Int32 i ) const { return m_he[i]; }
    
    Int32 size() const { return m_he.size(); }
    Int32 borders() const { return m_bordercount; }
    Int32 Build( const Mesh& mesh );
	void Reset();

	void ApplyHeatEq( MCData& mcdata, Real fTolerance );
    
    

	HEData();
	~HEData();
};

#endif
