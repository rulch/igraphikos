//
//  ViewControllerCommunicationDelegate.h
//  iGraphikos
//
//  Created by Ryan E. Ulch on 11/28/13.
//  Copyright (c) 2013 Ryan E. Ulch. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    DEMO_CATMULL_CLARK,
    DEMO_LOOP,
    DEMO_LAPLACIAN,
    DEMO_NONE
} DemoEnum_t;

typedef enum
{
    MESH_BUNNY_SMALL,
    MESH_CYLINDER,
    MESH_TEAPOT,
    MESH_CUBE,
    MESH_TRI_CUBE,
    MESH_OPEN_CUBE,
    MESH_TORUS,
    MESH_ROBOT,
    MESH_CONE,
    MESH_PLANE,
    MESH_NONE
} MeshEnum_t;


@protocol ViewControllerCommunicationDelegate <NSObject>

-(void)SwitchDemo:(DemoEnum_t)newDemo;
-(void)SwitchMesh:(MeshEnum_t)newMesh;
-(void)SetWireFrame:(BOOL)wireFrameOn;
-(void)SetShowSolid:(BOOL)showSolid;
-(void)SetLevel:(UInt16)level;
-(void)SetAnimationConstant:(float)animValue;
-(void)SetBorderHandling:(BOOL)standard;
-(void)SetShowPatchColors:(BOOL)colorsOn;
-(void)SetPickingMode:(BOOL)pickingModeOn;

@end