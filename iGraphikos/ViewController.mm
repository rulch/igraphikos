//
//  ViewController.m
//  iGraphikos
//
//  Created by Ryan E. Ulch on 11/19/13.
//  Copyright (c) 2013 Ryan E. Ulch. All rights reserved.
//

#import "ViewController.h"
#import "NavViewController.h"
#include "Mesh.h"
#include "GLMesh.h"
#include "HEData.h"
#include "SELinearMath.h"

#define __DEBUG__
#define BUFFER_OFFSET(i)    ((char *)NULL + (i))
#define RADIANS_PER_PIXEL   (M_PI / 320.f)
#define INITIAL_FOV         40
// Uniform index.
enum
{
    UNIFORM_MODELVIEWPROJECTION_MATRIX,
    UNIFORM_NORMAL_MATRIX,
    UNIFORM_WIREFRAME_ON,
    UNIFORM_RENDERING_WIREFRAME,
    NUM_UNIFORMS
    
};
GLint uniforms[NUM_UNIFORMS];

// Attribute index.
enum
{
    ATTRIB_VERTEX,
    ATTRIB_NORMAL,
    NUM_ATTRIBUTES
};


@interface ViewController () {
    // Rendering ivars
    GLuint          _program;
    
    GLKMatrix4      _modelViewProjectionMatrix;
    GLKMatrix3      _normalMatrix;
    
    GLuint          _vertexArray;
    GLuint          _vertexBuffer;
    
    // Meshes
    std::vector<Mesh>   _vMeshesConnectivity;
    std::vector<Mesh>   _vMeshesFinished;
    Mesh                _meshDraw;
    HEData              _meshHEData;
    
    Vector4         _colorBG;
    Vector4         _colorMesh;
    Vector4         _colorWire;
    Vector4         _colorOrigWire;
    
    GLMesh          _glmesh_solid;
    GLMesh          _glmesh_wire;
    GLMesh          _glmesh_wire_original;
    BOOL            _wireFrameOn;
    BOOL            _showSolid;
    BOOL            _showPatchColors;

    // For camera controls
    GLKQuaternion   _startQuaternion;
    GLKQuaternion   _currQuaternion;
    float           _fov;
    GLKVector3      _startPos;
    GLKVector3      _currPos;
    
    
    // Controls for subdivision/animation/display
    MeshEnum_t      _currMesh;
    DemoEnum_t      _currDemo;
    CGFloat         _animationValue;
    UInt16          _level;
    GLKVector3      _ray[2];
    bool            _renderRay;
    bool            _inPickingMode;
}

@property (strong, nonatomic) EAGLContext *context;
@property (strong, nonatomic) GLKBaseEffect *effect;

@property (weak, nonatomic) NavViewController *navViewController;

- (void)setupGL;
- (void)tearDownGL;

- (BOOL)loadShaders;
- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file;
- (BOOL)linkProgram:(GLuint)prog;
- (BOOL)validateProgram:(GLuint)prog;

@end

@implementation ViewController

- (GLKVector3) projectToSphere:(GLKVector3) pointOnScreen
{
    float radius = self.view.bounds.size.width/2;
    GLKVector3 center = GLKVector3Make(self.view.bounds.size.width/2, self.view.bounds.size.height/2, 0);
    GLKVector3 dToCenter = GLKVector3Subtract(pointOnScreen, center);
    
    // Flip y to account for screen in different coordinate space
    dToCenter = GLKVector3Make(dToCenter.x, -dToCenter.y, dToCenter.z);
    
    float sqRadius = radius * radius;
    float sqMagnitude = ( dToCenter.x * dToCenter.x )
                        + ( dToCenter.y * dToCenter.y );
    
    if (sqMagnitude <= sqRadius)
        dToCenter.z = sqrt(sqRadius - sqMagnitude);
    else
    {
        dToCenter.x *= (radius / sqrt(sqMagnitude));
        dToCenter.y *= (radius / sqrt(sqMagnitude));
        dToCenter.z = 0;
    }
    return GLKVector3Normalize(dToCenter);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];

    if (!self.context) {
        NSLog(@"Failed to create ES context");
    }

    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
    view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    _meshesInBundle = @[@"bunny_small",
                        @"cylinder",
                        @"teapot",
                        @"cube",
                        @"tri_cube",
                        @"open_cube",
                        @"torus",
                        @"robot",
                        @"cone",
                        @"plane"];
    [_uiDelegate SetMeshStringsRef:_meshesInBundle];
    
    [self setupGL];
    
    
    // Setting initial state
    _fov            = INITIAL_FOV;
    _animationValue = 0.0f;
    _wireFrameOn    = YES;
    _showSolid      = YES;
    _showPatchColors = NO;
    _renderRay      = false;
    _inPickingMode  = false;
    _currDemo       = DEMO_CATMULL_CLARK;
    _currMesh       = MESH_OPEN_CUBE;
    _level          = 0;
    
    // Setup colors
    _colorMesh      = Vector4( 0.6, 0.6, 1, 1 );
    _colorWire      = Vector4( 1.0, 0.2, 0, 1 );
    _colorBG        = Vector4( 0.85, 0.85, 0.85, 1 );
    _colorOrigWire  = Vector4( 0.1, 0.8, 0.3, 1 );
    
    // Use square as default

    [self LoadMeshes];
}

- (void)dealloc
{    
    [self tearDownGL];
    
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
}

-(BOOL)canBecomeFirstResponder {
    return YES;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [self resignFirstResponder];
    [super viewWillDisappear:animated];
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        _vMeshesFinished[_level].Randomize( 0.5f, 0.2f );
        [self SetAnimationConstant:_animationValue];
    }
    
    [self UpdateMeshes];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

    if ([self isViewLoaded] && ([[self view] window] == nil)) {
        self.view = nil;
        
        [self tearDownGL];
        
        if ([EAGLContext currentContext] == self.context) {
            [EAGLContext setCurrentContext:nil];
        }
        self.context = nil;

    }
    NSLog( @"Memory warning: FATAL!" );
}

- (void)setupGL
{
    [EAGLContext setCurrentContext:self.context];
    [self loadShaders];
    glEnable(GL_DEPTH_TEST);
    _currQuaternion = _startQuaternion = GLKQuaternionIdentity;
}

- (void)tearDownGL
{
    [EAGLContext setCurrentContext:self.context];
    
    glDeleteBuffers(1, &_vertexBuffer);
    glDeleteVertexArraysOES(1, &_vertexArray);
    
    self.effect = nil;
    
    if (_program) {
        glDeleteProgram(_program);
        _program = 0;
    }
}

#pragma mark - GLKView and GLKViewController delegate methods

- (void)update
{
    float aspect = fabsf(self.view.bounds.size.width / self.view.bounds.size.height);

    GLKMatrix4 projectionMatrix = GLKMatrix4MakePerspective(GLKMathDegreesToRadians(_fov), aspect, 1.0f, 10.0f);
    
    // Compute the model view matrix for the object rendered with ES2
    GLKMatrix4 modelViewMatrix = GLKMatrix4MakeWithQuaternion(_currQuaternion);
    GLKMatrix4 translationMatrix = GLKMatrix4MakeTranslation(0.0f, 0.0f, -4.0f);
    modelViewMatrix = GLKMatrix4Multiply(translationMatrix, modelViewMatrix);
    _normalMatrix = GLKMatrix3InvertAndTranspose(GLKMatrix4GetMatrix3(modelViewMatrix), NULL);
    _modelViewProjectionMatrix = GLKMatrix4Multiply(projectionMatrix, modelViewMatrix);
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    glClearColor(_colorBG.x, _colorBG.y, _colorBG.z, _colorBG.w);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    // Render the object again with ES2
    glUseProgram(_program);

    glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWPROJECTION_MATRIX], 1, 0, _modelViewProjectionMatrix.m);
    glUniformMatrix3fv(uniforms[UNIFORM_NORMAL_MATRIX], 1, 0, _normalMatrix.m);
    glUniform1f(uniforms[UNIFORM_WIREFRAME_ON], (GLfloat)_wireFrameOn);

    if( _wireFrameOn )
    {
        glUniform1f(uniforms[UNIFORM_RENDERING_WIREFRAME], (GLfloat)1.0f);
        _glmesh_wire.Prepare();
        _glmesh_wire.Draw();
        
        if( _animationValue > 0.02f || _level > 1 )
        {
            _glmesh_wire_original.Prepare();
            _glmesh_wire_original.Draw();
        }
    }

    if( _showSolid )
        glUniform1f(uniforms[UNIFORM_WIREFRAME_ON], (GLfloat)0.0f);
    else
        glUniform1f(uniforms[UNIFORM_WIREFRAME_ON], (GLfloat)1.0f);
    
    glUniform1f(uniforms[UNIFORM_RENDERING_WIREFRAME], (GLfloat)0.0f);
    _glmesh_solid.Prepare();
    _glmesh_solid.Draw();
    
    if( _renderRay )
    {
        GLfloat line[] = {  _ray[0].x, _ray[0].y, _ray[0].z,
                            _ray[1].x, _ray[1].y, _ray[1].z,
                            0, 1, 0 , 1, 1, 1};
        
        // Create an handle for a buffer object array
        GLuint bufferObjectNameArray;
        
        // Have OpenGL generate a buffer name and store it in the buffer object array
        glGenBuffers(1, &bufferObjectNameArray);
        
        // Bind the buffer object array to the GL_ARRAY_BUFFER target buffer
        glBindBuffer(GL_ARRAY_BUFFER, bufferObjectNameArray);
        
        // Send the line data over to the target buffer in GPU RAM
        glBufferData(
                     GL_ARRAY_BUFFER,   // the target buffer
                     sizeof(line),      // the number of bytes to put into the buffer
                     line,              // a pointer to the data being copied
                     GL_STATIC_DRAW);   // the usage pattern of the data
        
        // Enable vertex data to be fed down the graphics pipeline to be drawn
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(3));
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(6));

        // Set the line width
        glLineWidth(10.0);
        
        // Render the line
        glDrawArrays(GL_LINES, 0, 2);
    }
}



- (void) CalcRotationQuaternion {
    
    GLKVector3 currAxis = GLKVector3CrossProduct(_startPos, _currPos);
    float angle = acosf(GLKVector3DotProduct(_startPos, _currPos));
    
    GLKQuaternion quatRotation = GLKQuaternionMakeWithAngleAndVector3Axis(angle * 2, currAxis);
    quatRotation = GLKQuaternionNormalize(quatRotation);
    
    _currQuaternion = GLKQuaternionMultiply(quatRotation, _startQuaternion);
    
}

#pragma mark -  OpenGL ES 2 shader compilation

- (BOOL)loadShaders
{
    GLuint vertShader, fragShader;
    NSString *vertShaderPathname, *fragShaderPathname;
    
    // Create shader program.
    _program = glCreateProgram();
    
    // Create and compile vertex shader.
    vertShaderPathname = [[NSBundle mainBundle] pathForResource:@"Shader" ofType:@"vsh"];
    if (![self compileShader:&vertShader type:GL_VERTEX_SHADER file:vertShaderPathname]) {
        NSLog(@"Failed to compile vertex shader");
        return NO;
    }
    
    // Create and compile fragment shader.
    fragShaderPathname = [[NSBundle mainBundle] pathForResource:@"Shader" ofType:@"fsh"];
    if (![self compileShader:&fragShader type:GL_FRAGMENT_SHADER file:fragShaderPathname]) {
        NSLog(@"Failed to compile fragment shader");
        return NO;
    }
    
    // Attach vertex shader to program.
    glAttachShader(_program, vertShader);
    
    // Attach fragment shader to program.
    glAttachShader(_program, fragShader);
    
    // Bind attribute locations.
    // This needs to be done prior to linking.
    glBindAttribLocation(_program, GLKVertexAttribPosition, "position");
    glBindAttribLocation(_program, GLKVertexAttribNormal, "normal");
    glBindAttribLocation(_program, GLKVertexAttribColor, "color");
    
    // Link program.
    if (![self linkProgram:_program]) {
        NSLog(@"Failed to link program: %d", _program);
        
        if (vertShader) {
            glDeleteShader(vertShader);
            vertShader = 0;
        }
        if (fragShader) {
            glDeleteShader(fragShader);
            fragShader = 0;
        }
        if (_program) {
            glDeleteProgram(_program);
            _program = 0;
        }
        
        return NO;
    }
    
    // Get uniform locations.
    uniforms[UNIFORM_MODELVIEWPROJECTION_MATRIX] = glGetUniformLocation(_program, "modelViewProjectionMatrix");
    uniforms[UNIFORM_NORMAL_MATRIX] = glGetUniformLocation(_program, "normalMatrix");
    uniforms[UNIFORM_WIREFRAME_ON] = glGetUniformLocation(_program, "wireframeOn");
    uniforms[UNIFORM_RENDERING_WIREFRAME] = glGetUniformLocation(_program, "renderingWireFrame");

    // Release vertex and fragment shaders.
    if (vertShader) {
        glDetachShader(_program, vertShader);
        glDeleteShader(vertShader);
    }
    if (fragShader) {
        glDetachShader(_program, fragShader);
        glDeleteShader(fragShader);
    }
    
    return YES;
}

- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file
{
    GLint status;
    const GLchar *source;
    
    source = (GLchar *)[[NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:nil] UTF8String];
    if (!source) {
        NSLog(@"Failed to load vertex shader");
        return NO;
    }
    
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);
    
#if defined(DEBUG)
    GLint logLength;
    glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(*shader, logLength, &logLength, log);
        NSLog(@"Shader compile log:\n%s", log);
        free(log);
    }
#endif
    
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    if (status == 0) {
        glDeleteShader(*shader);
        return NO;
    }
    
    return YES;
}

- (BOOL)linkProgram:(GLuint)prog
{
    GLint status;
    glLinkProgram(prog);
    
#if defined(DEBUG)
    GLint logLength;
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program link log:\n%s", log);
        free(log);
    }
#endif
    
    glGetProgramiv(prog, GL_LINK_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    
    return YES;
}

- (BOOL)validateProgram:(GLuint)prog
{
    GLint logLength, status;
    
    glValidateProgram(prog);
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program validate log:\n%s", log);
        free(log);
    }
    
    glGetProgramiv(prog, GL_VALIDATE_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    
    return YES;
}


#pragma mark -  Gesture and touch handling
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch * touch = [touches anyObject];
    CGPoint positionInView = [touch locationInView:self.view];
    
    _startPos = GLKVector3Make(positionInView.x, positionInView.y, 0);
    _startPos = [self projectToSphere:_startPos];
    
    _currPos = _startPos;
    _startQuaternion = _currQuaternion;
}

- (IBAction)HandlePinchGesture:(UIPinchGestureRecognizer *)sender
{
    if ([sender state] == UIGestureRecognizerStateBegan || [sender state] == UIGestureRecognizerStateChanged)
    {
        _fov -= ( sender.velocity / 2.0f );
        _fov = ( _fov < 3.0f ) ? 3.0f : (_fov >= 160.0f ) ? 160.0f : _fov;
    }
    
}
- (IBAction)HandlePanGesture:(UIPanGestureRecognizer *)sender
{
    if( sender.numberOfTouches == 2  && _level > 0 )
    {
        CGFloat velocity = [sender velocityInView:self.view].x;
        _animationValue += velocity / 10000.0f;
        _animationValue  = _animationValue > 1.0f ? 1.0f :
                            _animationValue < 0.0f ? 0.0f : _animationValue;

        [self SetAnimationConstant:_animationValue];
        [_uiDelegate UpdateAnimationSlider:_animationValue];
        
    }
    else if( sender.numberOfTouches == 1 )
    {
        CGPoint location = [sender locationOfTouch:0 inView:self.view];
        
        _currPos = GLKVector3Make(location.x, location.y, 0);
        _currPos = [self projectToSphere:_currPos];
        
        [self CalcRotationQuaternion];
    }
}

void __gluMakeIdentityf(GLfloat m[16])
{
    m[0+4*0] = 1; m[0+4*1] = 0; m[0+4*2] = 0; m[0+4*3] = 0;
    m[1+4*0] = 0; m[1+4*1] = 1; m[1+4*2] = 0; m[1+4*3] = 0;
    m[2+4*0] = 0; m[2+4*1] = 0; m[2+4*2] = 1; m[2+4*3] = 0;
    m[3+4*0] = 0; m[3+4*1] = 0; m[3+4*2] = 0; m[3+4*3] = 1;
}

void gluLookAt(GLfloat eyex, GLfloat eyey, GLfloat eyez, GLfloat centerx,
               GLfloat centery, GLfloat centerz, GLfloat upx, GLfloat upy,
               GLfloat upz)
{
    Vector3 forward, side, up;
    GLfloat m[4][4];
    
    forward.x = centerx - eyex;
    forward.y = centery - eyey;
    forward.z = centerz - eyez;
    
    up.x = upx;
    up.y = upy;
    up.z = upz;
    
    forward = forward.Normalize();
    
    /* Side = forward x up */
    side = Vector3::Cross( forward, up );
    side = side.Normalize();
    
    /* Recompute up as: up = side x forward */
    up = Vector3::Cross(side, forward);
    
    __gluMakeIdentityf(&m[0][0]);
    m[0][0] = side[0];
    m[1][0] = side[1];
    m[2][0] = side[2];
    
    m[0][1] = up[0];
    m[1][1] = up[1];
    m[2][1] = up[2];
    
    m[0][2] = -forward[0];
    m[1][2] = -forward[1];
    m[2][2] = -forward[2];
    
    glMultMatrixf(&m[0][0]);
    glTranslatef(-eyex, -eyey, -eyez);
}

- (IBAction)HandleTapGesture:(UITapGestureRecognizer *)sender
{
    if( sender.state == UIGestureRecognizerStateEnded && _inPickingMode )
    {
        CGPoint point = [sender locationInView:self.view];
        point.y = (self.view.bounds.size.height - point.y);

        int viewPort[4];
        viewPort[0] = 0;
        viewPort[1] = 0;
        viewPort[2] = self.view.bounds.size.width;
        viewPort[3] = self.view.bounds.size.height;

    
        bool success = false;
        GLKVector3 window = GLKVector3Make(point.x, point.y, 0.0f);
        
        float aspect = fabsf(self.view.bounds.size.width / self.view.bounds.size.height);
        GLKMatrix4 projectionMatrix = GLKMatrix4MakePerspective(GLKMathDegreesToRadians(_fov), aspect, 1.0f, 10.0f);
        
        GLKMatrix4 modelViewMatrix = GLKMatrix4MakeWithQuaternion(_currQuaternion);
        GLKMatrix4 translationMatrix = GLKMatrix4MakeTranslation(0.0f, 0.0f, -4.0f);
        modelViewMatrix = GLKMatrix4Multiply(translationMatrix, modelViewMatrix);
        
        _ray[0] = GLKMathUnproject(window, modelViewMatrix, projectionMatrix, viewPort, &success);
        
        window.z = 1.0f;
        _ray[1] = GLKMathUnproject(window, modelViewMatrix, projectionMatrix, viewPort, &success);
        _renderRay = true;
        
        Vector3 rayOrigin, rayEnd;
        rayOrigin.x = _ray[0].x; rayOrigin.y = _ray[0].y; rayOrigin.z = _ray[0].z;
        rayEnd.x    = _ray[1].x; rayEnd.y    = _ray[1].y; rayEnd.z    = _ray[1].z;
        
        if( _vMeshesFinished[_level].IntersectionOccurs(rayOrigin, rayEnd, _meshHEData) )
        {
            _meshHEData.Build(_vMeshesFinished[_level]);
            if( _level == 0)
                _vMeshesConnectivity[0].Copy(_vMeshesFinished[0]);

            _meshDraw.Copy(_vMeshesFinished[_level]);
            [self UpdateMeshes];
        }
    }
}



#pragma mark - Demo reset/loading code

-(void)SwitchDemo:(DemoEnum_t)newDemo
{
    _currDemo = newDemo;
    _level = 0;
    _fov = INITIAL_FOV;
    [self LoadMeshes];
    // TODO: Switch demo type and reload meshes etc
}


-(void)SwitchMesh:(MeshEnum_t)newMesh
{
    // If index greater than or equal to count, return since it will go out of bounds
    if( newMesh >= [_meshesInBundle count] )
        return;
    
    _currMesh = newMesh;
    _fov = INITIAL_FOV;
    _level = 0;
    _currQuaternion = _startQuaternion = GLKQuaternionIdentity;
    [self LoadMeshes];
}

#pragma mark - Delegate handling code



-(void)SetWireFrame:(BOOL)wireFrameOn
{
    _wireFrameOn = wireFrameOn;
    [self UpdateMeshes];
}

-(void)SetLevel:(UInt16)level
{
    if( _level == level )
        return;
    
    if( _currDemo == DEMO_CATMULL_CLARK )
    {
        if( level < _level )
        {
            _vMeshesConnectivity.pop_back();
            _vMeshesFinished.pop_back();
            _meshDraw.Interpolate( _vMeshesConnectivity[level], _vMeshesFinished[level], _animationValue );
            _meshHEData.Build(_vMeshesFinished[level]);
        }
        else
        {
            Mesh mesh, meshSubdivided, meshSplit;
            mesh.Copy(_vMeshesFinished[_vMeshesFinished.size() - 1] );
            
            if( !Failed( meshSubdivided.CatmullSubdivision(mesh, &_meshHEData)))
            {
                meshSplit.CatmullSplit(mesh, &_meshHEData);
                _meshHEData.Build(meshSubdivided);
                
                _vMeshesConnectivity.push_back(meshSplit);
                _vMeshesFinished.push_back(meshSubdivided);
            }
            else
            {
                NSLog( @"Subdivision failure!" );
                return;
            }
            _level = level;
            _meshDraw.Interpolate( _vMeshesConnectivity[level], _vMeshesFinished[level], _animationValue );
        }
    }
    else if( _currDemo == DEMO_LAPLACIAN )
    {
        _meshDraw.Copy(_vMeshesFinished[_level]);
        // Connectivity mesh isn't set because we want to animate from level 0
        
        Mesh laplacian;
        laplacian.Laplacian( _meshDraw, &_meshHEData );
        _vMeshesFinished.push_back(laplacian);
    }
    else if( _currDemo == DEMO_LOOP )
    {
        if( level < _level )
        {
            _vMeshesConnectivity.pop_back();
            _vMeshesFinished.pop_back();
            _meshDraw.Interpolate( _vMeshesConnectivity[level], _vMeshesFinished[level], _animationValue );
            _meshHEData.Build(_vMeshesFinished[level]);
        }
        else
        {
            Mesh mesh, meshSubdivided, meshSplit;
            mesh.Copy(_vMeshesFinished[_vMeshesFinished.size() - 1] );
            
            if( !Failed( meshSubdivided.LoopSubdiv(mesh, &_meshHEData)))
            {
                meshSplit.LoopSplit(mesh, &_meshHEData);
                _meshHEData.Build(meshSubdivided);
                
                _vMeshesConnectivity.push_back(meshSplit);
                _vMeshesFinished.push_back(meshSubdivided);
            }
            else
            {
                NSLog( @"Subdivision failure!" );
                return;
            }
            _level = level;
            _meshDraw.Interpolate( _vMeshesConnectivity[level], _vMeshesFinished[level], _animationValue );
        }
    }
    else
        return;
    
    _level = level;
    [self UpdateMeshes];
}

-(void)SetShowSolid:(BOOL)showSolid
{
    _showSolid = showSolid;
    [self UpdateMeshes];
}


-(void)SetShowPatchColors:(BOOL)colorsOn
{
    _showPatchColors = colorsOn;
    [self UpdateMeshes];
}

-(void)SetBorderHandling:(BOOL)standard
{
    for( int i = 0; i <= _level; ++i )
    {
        _vMeshesConnectivity[i].SetStandardBorders(standard);
        _vMeshesFinished[i].SetStandardBorders(standard);
    }
    
    [self SetAnimationConstant:_animationValue];
}

-(void)SetAnimationConstant:(float)animValue
{
    _animationValue = animValue;
    int connectivityLevel = _currDemo == DEMO_LAPLACIAN ? 0 : _level;
    _meshDraw.Interpolate( _vMeshesConnectivity[connectivityLevel], _vMeshesFinished[_level], _animationValue );
    [self UpdateMeshes];
}

-(void)SetPickingMode:(BOOL)pickingModeOn
{
    _inPickingMode = pickingModeOn;
}

-(void)UpdateMeshes
{
    if( !_showSolid )
        _glmesh_solid.FromMesh( _meshDraw, 0, _colorBG, false);
    else if( !_showPatchColors )
        _glmesh_solid.FromMesh( _meshDraw, 0, _colorMesh, false );
    else
        _glmesh_solid.FromMesh( _meshDraw, 0, _colorMesh, true );
    
    _glmesh_wire.FromMesh( _meshDraw, 1, _colorWire, false );
}

-(void)LoadMeshes
{
    if( _currMesh >= [_meshesInBundle count] )
    {
        NSLog(@"Error: Attempted to load out of bounds mesh!");
        return;
    }
    _vMeshesConnectivity.clear();
    _vMeshesFinished.clear();
    
    NSString* myFile = [[NSBundle mainBundle] pathForResource: _meshesInBundle[_currMesh] ofType: @"obj"];
    std::string filename( myFile.UTF8String );
    Mesh mesh;
    mesh.OpenFile( filename );
    if( _currDemo == DEMO_LOOP )
    {
        Mesh temp;
        temp.Copy(mesh);
        mesh.Triangulate(temp);
    }
    _meshHEData.Build(mesh);
    
    _vMeshesConnectivity.push_back(mesh);
    _vMeshesFinished.push_back(mesh);
    
    [self SetAnimationConstant:_animationValue];
    _glmesh_wire_original.FromMesh( _meshDraw, 1, _colorOrigWire );
}
@end





































