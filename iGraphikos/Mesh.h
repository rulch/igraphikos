#ifndef __MESH__
#define __MESH__

#include "SELinearMath.h"

#include <vector>
#include <string>

struct Index
{
	Int32 v, t, n, c;    // added hard indicator here
    
    Index() {}
    
    Index( Int32 iv, Int32 ic = -1 )
    {
        v = iv;
        t = -1;
        n = -1;
        c = ic;
    }
};

struct VertexInfo
{
    Vector3 pos;
    bool    hard;
};

typedef std::vector<Vector3> MCData;

class HEData;

class Mesh
{
protected:
	Int32					m_culling;
	Int32					m_wireframe;
	Int32					m_invnorm;

	std::vector<VertexInfo>     m_pos;
	std::vector<Vector2>	m_tex;
	std::vector<Vector3>	m_norm;

	std::vector<Index>		m_tri;
	std::vector<Index>		m_quad;

	Vector3					m_min;
	Vector3					m_max;

	Vector3					m_moved;
	Real					m_scaled;
    
    Int32                   m_standardBorders;

	void _CalcNormals();
	void _FixSize();

public:
	Int32 ToggleCullSide();
	Int32 ToggleWireframe();
	Int32 ToggleInvNorm();

	const std::vector<VertexInfo>& pos() const { return m_pos; }

	const std::vector<Vector2>& tex() const { return m_tex; }
	const std::vector<Vector3>& norm() const { return m_norm; }
	const std::vector<Index>& tri() const { return m_tri; }
	const std::vector<Index>& quad() const { return m_quad; }
    void SetStandardBorders( bool standard ) { m_standardBorders = standard; }
    
    bool IntersectionOccurs( Vector3 rayOrigin, Vector3 rayEnd, const HEData& he );
    
	Int32 GetTriCount() const { return m_tri.size()/3; }
	Int32 GetQuadCount() const { return m_quad.size()/4; }

	Int32 GetPolyCount() const { return GetTriCount() + GetQuadCount(); }
	Int32 GetPoly( Int32 i, Index poly[4] ) const;
    
    Int32 Copy( const Mesh& m );
 
	Int32 OpenFile( std::string& filename );
	void Reset();
    
    Int32 CatmullSplit( const Mesh& m, const HEData* pHEin );
    Int32 CatmullSubdivision( const Mesh& m, const HEData* pHEin );
    Int32 Interpolate( const Mesh& m, const Mesh& m2, float animationValue );
    
    Int32 Laplacian( const Mesh& m, const HEData* pHEin );
    
    Int32 Triangulate( const Mesh& m );
    Int32 LoopSubdiv( const Mesh& m, const HEData* pHEin );
    Int32 LoopSplit( const Mesh& m, const HEData* pHEin );
    
    Int32 Randomize( Real fPercent, Real fForce );

	Mesh();
	~Mesh();
};

#endif
