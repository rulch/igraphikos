//
//  InformationViewController.m
//  iGraphikos
//
//  Created by Ryan E. Ulch on 12/3/13.
//  Copyright (c) 2013 Ryan E. Ulch. All rights reserved.
//

#import "InformationViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface InformationViewController ()

@end

@implementation InformationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.modalPresentationStyle = UIModalPresentationFormSheet;
        self.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    }
    return self;
}


-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //resize modal view
    self.view.superview.bounds = CGRectMake(0, 0, 800, 680);
    self.view.layer.cornerRadius = 12.0f;
    self.view.superview.layer.cornerRadius = 12.0f;
    [[self.mGeneralOverviewText layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[self.mGeneralOverviewText layer] setBorderWidth:2.5];
    [[self.mGeneralOverviewText layer] setCornerRadius:12];
    
    [[self.mSecondaryText layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[self.mSecondaryText layer] setBorderWidth:2.5];
    [[self.mSecondaryText layer] setCornerRadius:12];
}


-(void) viewDidAppear:(BOOL)animated
{
    CGPoint centerPoint = CGPointMake([[UIScreen mainScreen] bounds].size.width/2, [[UIScreen mainScreen] bounds].size.height/2);
    self.view.superview.center = centerPoint;
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(HandleTapOutside:)];
    
    [recognizer setNumberOfTapsRequired:1];
    recognizer.cancelsTouchesInView = NO; // dont override other touches in view
    [self.view.window addGestureRecognizer:recognizer];
}

- (void)HandleTapOutside:(UITapGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        CGPoint location = [sender locationInView:nil]; //nil gives window coords
        
        if (![self.view pointInside:[self.view convertPoint:location fromView:self.view.window] withEvent:nil])
        {
            // Remove the recognizer first so it's view.window is valid.
            [self.view.window removeGestureRecognizer:sender];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)CloseView:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)GeneralLink1:(UIButton *)sender
{
    NSString* urlString;
    
    if( _demoType == DEMO_CATMULL_CLARK )
        urlString = @"http://wiki.polycount.com/SubdivisionSurfaceModeling";
    else if( _demoType == DEMO_LOOP )
        urlString = @"http://wiki.polycount.com/SubdivisionSurfaceModeling";
    else if( _demoType == DEMO_LAPLACIAN )
        urlString = @"http://en.wikipedia.org/wiki/Laplacian_smoothing"; // Add link here
    else
    {
        NSLog(@"Invalid demo type!" );
        return;
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
}

- (IBAction)GeneralLink2:(UIButton *)sender
{
    NSString* urlString;
    
    if( _demoType == DEMO_CATMULL_CLARK )
        urlString = @"http://www.rorydriscoll.com/2008/08/01/catmull-clark-subdivision-the-basics/";
    else if( _demoType == DEMO_LOOP )
        urlString = @"http://russellstimpson.com/LoopSubdivision.html";
    else if( _demoType == DEMO_LAPLACIAN )
        urlString = @"http://www.cs.jhu.edu/~misha/Fall07/Papers/Nealen06.pdf"; // Add link here
    else
    {
        NSLog(@"Invalid demo type!" );
        return;
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
}

- (IBAction)GeneralLink3:(UIButton *)sender
{
    NSString* urlString;
    
    if( _demoType == DEMO_CATMULL_CLARK )
        urlString = @"http://www.cs.cornell.edu/courses/cs4620/2009fa/lectures/01subdivision.pdf";
    else if( _demoType == DEMO_LOOP )
        urlString = @"http://www.cs.cornell.edu/courses/cs4620/2009fa/lectures/01subdivision.pdf";
    else if( _demoType == DEMO_LAPLACIAN )
        urlString = @"http://www.google.com";
    else
    {
        NSLog(@"Invalid demo type!" );
        return;
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
}

- (IBAction)SecondaryLink1:(UIButton *)sender
{
    NSString* urlString;
    
    if( _demoType == DEMO_CATMULL_CLARK )
        urlString = @"http://en.wikipedia.org/wiki/Doubly_connected_edge_list";
    else if( _demoType == DEMO_LOOP )
        urlString = @"http://en.wikipedia.org/wiki/Doubly_connected_edge_list";
    else if( _demoType == DEMO_LAPLACIAN )
        urlString = @"http://www.google.com";
    else
    {
        NSLog(@"Invalid demo type!" );
        return;
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
}

- (IBAction)SecondaryLink2:(UIButton *)sender
{
    NSString* urlString;
    
    if( _demoType == DEMO_CATMULL_CLARK )
        urlString = @"http://www.flipcode.com/archives/The_Half-Edge_Data_Structure.shtml";
    else if( _demoType == DEMO_LOOP )
        urlString = @"http://www.flipcode.com/archives/The_Half-Edge_Data_Structure.shtml";
    else if( _demoType == DEMO_LAPLACIAN )
        urlString = @"http://www.google.com";
    else
    {
        NSLog(@"Invalid demo type!" );
        return;
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
}

- (IBAction)TriggerHalfEdgeInfo:(UIButton *)sender
{
    _mHalfEdgeInfo.modalPresentationStyle = UIModalPresentationFormSheet;
    _mHalfEdgeInfo.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    [self presentViewController:_mHalfEdgeInfo animated:NO completion:nil];
}

@end
