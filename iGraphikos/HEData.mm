#include "HEData.h"

HEData::HEData()
{
	Reset();
}

HEData::~HEData()
{
	Reset();
}

Int32 HEData::Build( const Mesh& mesh )
{
	Reset();

	HalfEdge he;
	he.pair = -1;
		
	Int32 count, first;
	Index poly[4];
	for( Int32 p = 0; p < mesh.GetPolyCount(); p++ )
	{
		count = mesh.GetPoly( p, poly );
		first = m_he.size();
		for( Int32 i = 0; i < count; i++ )
		{
			he.f = p;
			he.v = poly[i].v;
            he.hard = mesh.pos()[poly[i].v].hard;                                 // New line for hardness

			he.next = i + 1 == count ? first : first + i + 1;
			m_he.push_back( he );
		}
	}

	std::vector<Int32> list;
	list.reserve( 10000 );

	for( Int32 e1 = 0; e1 < m_he.size(); e1++ )
	{
		if( m_he[e1].pair < 0 )
		{
			for( Int32 i = 0; i < list.size(); i++ )
			{
				Int32& e2 = list[i];
				if( m_he[m_he[e1].next].v == m_he[e2].v &&
					m_he[m_he[e2].next].v == m_he[e1].v )
				{
                    m_he[e1].pair = e2;
                    m_he[e2].pair = e1;

					list.erase( list.begin() + i );
					break;
				}
			}
			if( m_he[e1].pair < 0 )
				list.push_back( e1 );
		}
	}

	m_bordercount = 0;
	for( Int32 e1 = 0; e1 < m_he.size(); e1++ )
	{
		if( m_he[e1].pair < 0 )
			m_bordercount++;
	}

	m_pMesh = &mesh;

	return _OK;
}

void HEData::Reset()
{
	m_pMesh = 0;
	m_he.clear();
}

Int32 HEData::FirstNeighbor( Int32 h )
{
	return h < 0 ? -1 : m_he[h].pair;
}

Int32 HEData::NextNeighbor( Int32 h )
{
	if( h < 0 ) return -1;
	return m_he[h].next < 0 ? -1 : m_he[m_he[h].next].pair;
}
