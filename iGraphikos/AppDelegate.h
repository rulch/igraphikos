//
//  AppDelegate.h
//  iGraphikos
//
//  Created by Ryan E. Ulch on 11/19/13.
//  Copyright (c) 2013 Ryan E. Ulch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
