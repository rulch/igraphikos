//
//  Shader.vsh
//  iGraphikos
//
//  Created by Ryan E. Ulch on 11/19/13.
//  Copyright (c) 2013 Ryan E. Ulch. All rights reserved.
//

attribute vec4 position;
attribute vec3 normal;
attribute vec4 color;

varying lowp float dotVarying;
varying lowp vec4 colorVarying;

uniform mat4 modelViewProjectionMatrix;
uniform mat3 normalMatrix;

uniform lowp float wireframeOn;
uniform lowp float renderingWireFrame;

uniform vec4 originalPosition;

void main()
{
    vec3 eyeNormal = normalize(normalMatrix * normal);
    vec4 pos = position;
    
    vec3 lightPosition = vec3(0.0, 0.0, 1.0);
    
    float nDotVP = dot(eyeNormal, normalize(lightPosition));
    
    // Combat z-fighting
    if( renderingWireFrame > 0.5 )
    {
        if( nDotVP < 0.0 )
        {
            pos[0] = pos[0] - (normal[0] * 0.004);
            pos[1] = pos[1] - (normal[1] * 0.004);
            pos[2] = pos[2] - (normal[2] * 0.004);
        }
        else
        {
            pos[0] = pos[0] + (normal[0] * 0.004);
            pos[1] = pos[1] + (normal[1] * 0.004);
            pos[2] = pos[2] + (normal[2] * 0.004);
        }
    }
    
    nDotVP = max( 0.0, abs( nDotVP ));
    
    colorVarying = color;
    
    dotVarying = wireframeOn > 0.5 ? 1.0 : nDotVP;
    
    gl_Position = modelViewProjectionMatrix * pos;
}
