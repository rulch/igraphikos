//
//  Shader.fsh
//  iGraphikos
//
//  Created by Ryan E. Ulch on 11/19/13.
//  Copyright (c) 2013 Ryan E. Ulch. All rights reserved.
//

varying lowp vec4 colorVarying;
varying lowp float dotVarying;

void main()
{
    if( dotVarying < 0.99 || dotVarying > 1.01 )
        gl_FragColor = colorVarying * dotVarying;
    else
        gl_FragColor = colorVarying;
}
