//
//  ViewController.h
//  iGraphikos
//
//  Created by Ryan E. Ulch on 11/19/13.
//  Copyright (c) 2013 Ryan E. Ulch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import <CoreMotion/CMMotionManager.h>
#import "UpdateUIDelegate.h"
#import "ViewControllerCommunicationDelegate.h"


@interface ViewController : GLKViewController <ViewControllerCommunicationDelegate>

@property (strong, nonatomic) IBOutlet GLKView *glView;
@property (strong, nonatomic) NSArray* meshesInBundle;
// Gesture stuff
@property (strong, nonatomic) IBOutlet UIPanGestureRecognizer *mPanGestureRecognizer;
@property (nonatomic, weak) id<UpdateUIDelegate> uiDelegate;

- (IBAction)HandlePanGesture:(UIPanGestureRecognizer *)sender;
- (IBAction)HandlePinchGesture:(UIPinchGestureRecognizer *)sender;
- (IBAction)HandleTapGesture:(UITapGestureRecognizer *)sender;

@property (strong, nonatomic) CMMotionManager* motionManager;


@end
