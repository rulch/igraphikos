//
//  GLMesh.h
//  iGraphikos
//
//  Created by Ryan E. Ulch on 11/20/13.
//  Copyright (c) 2013 Ryan E. Ulch. All rights reserved.
//

#ifndef __iGraphikos__GLMesh__
#define __iGraphikos__GLMesh__

#include "SELinearMath.h"
#include "Mesh.h"

#import <GLKit/GLKit.h>

struct Vertex
{
    Vector3 pos;
    Vector3 norm;
    Vector4 color;
};

class GLMesh
{
private:
    int          m_bLines;
    GLuint       m_glVBuffer;
    Int32        m_nVertexCount;
    
public:    
    int FromMesh( const Mesh& m, int bLines, Vector4 vColor, BOOL bColor = YES );
    int Prepare();
    int Draw();
    
    GLMesh();
    ~GLMesh();
};



#endif /* defined(__iGraphikos__GLMesh__) */
