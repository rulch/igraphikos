//
//  NavViewController.h
//  iGraphikos
//
//  Created by Ryan E. Ulch on 11/24/13.
//  Copyright (c) 2013 Ryan E. Ulch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "ViewControllerCommunicationDelegate.h"
#import "UpdateUIDelegate.h"
#import "InformationViewController.h"



@interface NavViewController : UIViewController <UIActionSheetDelegate, UIAlertViewDelegate, UpdateUIDelegate>

// Outlets
@property (weak, nonatomic) IBOutlet UIButton*                  demoButton;
@property (weak, nonatomic) IBOutlet UIButton*                  meshButton;

@property (weak, nonatomic) IBOutlet UIStepper*                 levelStepper;
@property (weak, nonatomic) IBOutlet UILabel*                   levelCounterLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView*   levelProgressIndicator;

@property (weak, nonatomic) IBOutlet UISwitch*                  borderSwitch;
@property (weak, nonatomic) IBOutlet UILabel*                   borderLabel;

@property (weak, nonatomic) IBOutlet UISwitch*                  mShowPatchColors;
@property (weak, nonatomic) IBOutlet UISlider*                  animationSlider;
@property (weak, nonatomic) IBOutlet UIButton*                  helpButton;

@property (weak, nonatomic) IBOutlet UISwitch*                  edgeModeSwitch;
@property (weak, nonatomic) IBOutlet UILabel*                   edgeLabel;
@property (weak, nonatomic) IBOutlet UISwitch *wireframeSwitch;

// UI Actions
- (IBAction)ToggleWireFrame:(UISwitch *)sender;
- (IBAction)ShowDemoChoices:(UIButton *)sender;
- (IBAction)ShowMeshChoices:(UIButton *)sender;
- (IBAction)HandleLevelStepper:(UIStepper *)sender;
- (IBAction)HandleAnimationController:(UISlider *)sender;
- (IBAction)TriggerHelpMenu:(id)sender;
- (IBAction)ToggleShowSolid:(UISwitch *)sender;
- (IBAction)ToggleBorderHandling:(UISwitch *)sender;
- (IBAction)ToggleShowPatchColors:(UISwitch *)sender;
- (IBAction)ToggleEdgeMode:(UISwitch *)sender;

- (IBAction)ShowMoreInformation:(id)sender;
- (IBAction)ShowSourceCodeLink:(UIButton *)sender;


// Other various properties
@property (nonatomic, weak) id<ViewControllerCommunicationDelegate> communicationDelegate;
@property (nonatomic, weak) NSArray* meshStrings;
@property (nonatomic, weak) id glViewController;

@property (nonatomic, weak) InformationViewController*      mInformationViewController;
@property (nonatomic, strong) InformationViewController*    mCatmullInfoController;
@property (nonatomic, strong) InformationViewController*    mLoopInfoController;
@property (nonatomic, strong) InformationViewController*    mLaplacianInfoController;
@property (nonatomic, strong) InformationViewController*    mHalfEdgeInfoController;


@end
