#include "HEData.h"
#include "Geometry.h"

#include <gl\GL.h>
#include <gl\GLU.h>

HEData::HEData()
{
	Reset();
}

HEData::~HEData()
{
	Reset();
}

Int32 HEData::Build( const Mesh& mesh )
{
	Reset();

	HalfEdge he;
	he.pair = -1;
		
	Int32 count, first;
	Index poly[4];
	for( Int32 p = 0; p < mesh.GetPolyCount(); p++ )
	{
		count = mesh.GetPoly( p, poly );
		first = m_he.size();
		for( Int32 i = 0; i < count; i++ )
		{
			he.f = p;
			he.v = poly[i].v;
			he.next = i + 1 == count ? first : first + i + 1;
			m_he.push_back( he );
		}
	}

	std::vector<Int32> list;
	list.reserve( 10000 );

	for( Int32 e1 = 0; e1 < m_he.size(); e1++ )
	{
		if( m_he[e1].pair < 0 )
		{
			for( Int32 i = 0; i < list.size(); i++ )
			{
				Int32& e2 = list[i];
				if( m_he[m_he[e1].next].v == m_he[e2].v &&
					m_he[m_he[e2].next].v == m_he[e1].v )
				{
					m_he[e1].pair = e2;
					m_he[e2].pair = e1;
					list.erase( list.begin() + i );
					break;
				}
			}

			if( m_he[e1].pair < 0 )
				list.push_back( e1 );
		}
	}

	Int32 nCount = 0;
	for( Int32 e1 = 0; e1 < m_he.size(); e1++ )
	{
		if( m_he[e1].pair < 0 )
			nCount++;
	}
	DEBUGOUT( "No Pairs: %d\n", nCount );

	m_pMesh = &mesh;

	return _OK;
}

void HEData::Reset()
{
	m_pMesh = 0;
	m_he.clear();
}

Int32 HEData::FirstNeighbor( Int32 h )
{
	return h < 0 ? -1 : m_he[h].pair;
}

Int32 HEData::NextNeighbor( Int32 h )
{
	if( h < 0 ) return -1;
	return m_he[h].next < 0 ? -1 : m_he[m_he[h].next].pair;
}

void HEData::ApplyHeatEq( MCData& mcdata, Real fTolerance )
{
	if( !m_pMesh )
		return;

	const Mesh& m = *m_pMesh;

	mcdata.clear();
	mcdata.reserve( m.pos().size() );
	for( Int32 i = 0; i < m.pos().size(); i++ )
		mcdata.push_back( Vector3( 0, 0, 0 ) );

	Int32 hs, ha, hb, hc, va, vb, vc;
	Vector3 a, b, c, v1, v2;
	Real angle;

	for( Int32 i = 0; i < m.pos().size(); i++ )
	{
		for( ha = 0; ha < m_he.size(); ha++ )
			if( m_he[ha].v == i )
				break;
		if( ha == m_he.size() )
			continue;

		hs = hb = FirstNeighbor( ha );
		hc = NextNeighbor( hb );

		if( ha < 0 || hb < 0 || hc < 0 )
		{
			mcdata[i] = Vector3( 0, 0.5f, 0 );	// Edge Vertex
			continue;
		}

		va = m_he[ha].v;
		a = m.pos()[va];

		vb = m_he[hb].v;
		b = m.pos()[vb] - a;

		v1 = b.GetNormal();
		
		angle = 0;

		do
		{
			hc = NextNeighbor( hb );
			if( hc < 0 )
			{
				mcdata[i] = Vector3( 0, 0.5f, 0 );	// Edge Vertex
				break;
			}

			vc = m_he[hc].v;
			c = m.pos()[vc] - a;

			v2 = c.GetNormal();
			angle += ACOS( Vector3::Dot( v1, v2 ) );

			v1 = v2;

			hb = hc;
			vb = vc;
			b = c;

		}
		while( hc != hs );

		if( hc >= 0 )
		{
			angle = ABS( ( 2.0f * PI - angle ) ) / fTolerance;
			mcdata[i] = Vector3( angle, 0, 0 );
//			DEBUGOUT( "angle: %f\n", angle );
		}
	}
}
