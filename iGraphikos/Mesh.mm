#include "Mesh.h"
#include "HEData.h"

#include <fstream>

Mesh::Mesh()
{
	Reset();
    m_standardBorders = true;
}

Mesh::~Mesh()
{
	Reset();
}


// Open obj file
Int32 Mesh::OpenFile( std::string& filename )
{
	Reset();

	std::ifstream objfile;
	objfile.open( filename );

	if( !objfile.is_open() )
		return _ERR;

    Int32 poly = 1;
	Index a, b, c, d;
	Vector3 pos, norm;
	Vector2 tex;

	char buffer[256];
	while( !objfile.eof() )
	{
		objfile.getline( buffer, 256 );

		if( buffer[0] == '#' )
			continue;
		else if( buffer[0] == 'v' )
		{
			if( buffer[1] == ' ' )
			{
                // get vertex positions
				sscanf( buffer, "v %f %f %f", &pos.x, &pos.y, &pos.z );

				if( m_pos.size() == 0 )
				{
					m_min = pos;
					m_max = pos;
				}
				else
				{
					if( pos.x < m_min.x ) m_min.x = pos.x;
					if( pos.y < m_min.y ) m_min.y = pos.y;
					if( pos.z < m_min.z ) m_min.z = pos.z;
					if( pos.x > m_max.x ) m_max.x = pos.x;
					if( pos.y > m_max.y ) m_max.y = pos.y;
					if( pos.z > m_max.z ) m_max.z = pos.z;
				}
                VertexInfo v;
                v.pos = pos; v.hard = false;
				m_pos.push_back( v );
			}
			else if( buffer[1] == 't' )
			{
                // get texture coordinates
				sscanf( buffer, "vt %f %f", &tex.x, &tex.y );
				m_tex.push_back( tex );
			}
			else if( buffer[1] == 'n' )
			{
                // get normals
				sscanf( buffer, "vn %f %f %f", &norm.x, &norm.y, &norm.z );
				m_norm.push_back( norm );
			}
		}
		else if( buffer[0] == 'f' ) // faces
		{
			int nSlash = 0;
			int nCount = 0;
			int bEnable = 0;
			for( int i = 0; buffer[i] != '\0'; i++ )
			{
				if( buffer[i] != ' ' )
				{
					if( bEnable )
					{
						nCount++;
						bEnable = 0;
					}
				}
				else
					bEnable = 1;

				if( buffer[i] == '/' )
					nSlash++;
			}

			if( nCount == 4 ) // If we're dealing with quads...
			{
                // if vertices, textures and normals are defined...
				if( m_tex.size() > 0 && m_norm.size() > 0 )
				{
					sscanf( buffer, "f %d/%d/%d %d/%d/%d %d/%d/%d %d/%d/%d", 
						&a.v, &a.t, &a.n, &b.v, &b.t, &b.n, &c.v, &c.t, &c.n, &d.v, &d.t, &d.n );
				}
				else if( m_norm.size() > 0 ) // if just norms and vertices are defined
				{
					if( nSlash > 4 )
						sscanf( buffer, "f %d//%d %d//%d %d//%d %d//%d", &a.v, &a.n, &b.v, &b.n, &c.v, &c.n, &d.v, &d.n );
					else
						sscanf( buffer, "f %d/%d %d/%d %d/%d %d/%d", &a.v, &a.n, &b.v, &b.n, &c.v, &c.n, &d.v, &d.n );
					a.t = b.t = c.t = d.t = 0;
				}
				else if( m_tex.size() > 0 ) // if just vertices and texture coords are defined
				{
					if( nSlash > 4 )
						sscanf( buffer, "f %d//%d %d//%d %d//%d %d//%d", &a.v, &a.t, &b.v, &b.t, &c.v, &c.t, &d.v, &d.t );
					else
						sscanf( buffer, "f %d/%d %d/%d %d/%d %d/%d", &a.v, &a.t, &b.v, &b.t, &c.v, &c.t, &d.v, &d.t );
					a.n = b.n = c.n = d.n = 0;
				}
				else // if just vertices are defined...
				{
					sscanf( buffer, "f %d %d %d %d", &a.v, &b.v, &c.v, &d.v );
					a.t = b.t = c.t = d.t = 0;
					a.n = b.n = c.n = d.n = 0;
				}
				a.v--; b.v--; c.v--; d.v--;
				a.t--; b.t--; c.t--; d.t--;
				a.n--; b.n--; c.n--; d.n--;
                a.c = b.c = c.c = d.c = poly++;

				m_quad.push_back( a );
				m_quad.push_back( b );
				m_quad.push_back( c );
				m_quad.push_back( d );
			}
			else if( nCount == 3 ) // If we're just dealing with tris
			{
				if( m_tex.size() > 0 && m_norm.size() > 0 )
				{
					sscanf( buffer, "f %d/%d/%d %d/%d/%d %d/%d/%d", 
						&a.v, &a.t, &a.n, &b.v, &b.t, &b.n, &c.v, &c.t, &c.n );
				}
				else if( m_norm.size() > 0 )
				{
					sscanf( buffer, "f %d/%d %d/%d %d/%d", &a.v, &a.n, &b.v, &b.n, &c.v, &c.n );
					a.t = b.t = c.t = 0;
				}
				else if( m_tex.size() > 0 )
				{
					sscanf( buffer, "f %d/%d %d/%d %d/%d", &a.v, &a.t, &b.v, &b.t, &c.v, &c.t );
					a.n = b.n = c.n = 0;
				}
				else
				{
					sscanf( buffer, "f %d %d %d", &a.v, &b.v, &c.v );
					a.t = b.t = c.t = 0;
					a.n = b.n = c.n = 0;
				}
				a.v--; b.v--; c.v--;
				a.t--; b.t--; c.t--;
				a.n--; b.n--; c.n--;
                a.c = b.c = c.c = poly++;

				m_tri.push_back( a );
				m_tri.push_back( b );
				m_tri.push_back( c );
			}
		}
	}
	objfile.close();

	if( m_norm.size() == 0 )
		_CalcNormals();

	_FixSize();

	return _OK;
}


// Clear all data
void Mesh::Reset()
{
	m_culling = 0;
	m_wireframe = 0;
	m_invnorm = 0;
	m_pos.clear();
	m_tex.clear();
	m_norm.clear();
	m_tri.clear();
	m_quad.clear();
	m_min.Zero();
	m_max.Zero();
}

// Calculate vertex normals
void Mesh::_CalcNormals()
{
	if( m_pos.size() == 0 ) 
		return;

	Vector3 v1, v2, norm = Vector3( 0, 0, 0 );
	m_norm.reserve( m_pos.size() );
	for( Int32 i = 0; i < m_pos.size(); i++ )
		m_norm.push_back( norm );

    // Calculate normals for tris
	for( int i = 0; i < m_tri.size() / 3; i++ )
	{
		Index& a = m_tri[i*3];
		Index& b = m_tri[i*3+1];
		Index& c = m_tri[i*3+2];

		a.n = a.v;
		b.n = b.v;
		c.n = c.v;

		v1 = m_pos[b.v].pos - m_pos[a.v].pos;
		v2 = m_pos[c.v].pos - m_pos[a.v].pos;

		norm = Vector3::Cross( v1, v2 ).GetNormal();

		m_norm[a.v] += norm;
		m_norm[b.v] += norm;
		m_norm[c.v] += norm;
	}

    // Calculate normals for quads
	for( int i = 0; i < m_quad.size() / 4; i++ )
	{
		Index& a = m_quad[i*4];
		Index& b = m_quad[i*4+1];
		Index& c = m_quad[i*4+2];
		Index& d = m_quad[i*4+3];

		a.n = a.v;
		b.n = b.v;
		c.n = c.v;
		d.n = d.v;

		v1 = m_pos[b.v].pos - m_pos[a.v].pos;
		v2 = m_pos[c.v].pos - m_pos[a.v].pos;

		norm = Vector3::Cross( v1, v2 ).GetNormal();

		m_norm[a.v] += norm;
		m_norm[b.v] += norm;
		m_norm[c.v] += norm;
		m_norm[d.v] += norm;
	}

	for( Int32 i = 0; i < m_norm.size(); i++ )
		m_norm[i].Normalize();
}


void Mesh::_FixSize()
{
	Vector3 diff = m_max - m_min;
	Vector3 move = m_min + diff * 0.5f;

	Real scale;
	if( diff.x > diff.y )
		scale = 1.0f / ( diff.x > diff.z ? diff.x : diff.z );
	else
		scale = 1.0f / ( diff.y > diff.z ? diff.y : diff.z );

	for( Int32 i = 0; i < m_pos.size(); i++ )
	{
		m_pos[i].pos -= move;
		m_pos[i].pos *= scale;
	}

	m_moved = move;
	m_scaled = scale;
}

// Return face indices at i
Int32 Mesh::GetPoly( Int32 i, Index poly[4] ) const
{
	if( i < m_tri.size()/3 )
	{
		i *= 3;
		poly[0] = m_tri[i];
		poly[1] = m_tri[i+1];
		poly[2] = m_tri[i+2];
		return 3;
	}
	else
	{
		i -= m_tri.size()/3;
		i *= 4;
		poly[0] = m_quad[i];
		poly[1] = m_quad[i+1];
		poly[2] = m_quad[i+2];
		poly[3] = m_quad[i+3];
		return 4;
	}
}

Int32 Mesh::ToggleCullSide()
{
	m_culling++;
	if( m_culling == 3 )
		m_culling = 0;
	return m_culling;
}

Int32 Mesh::ToggleWireframe()
{
	m_wireframe = !m_wireframe;
	return m_wireframe;
}

Int32 Mesh::ToggleInvNorm()
{
	m_invnorm = !m_invnorm;
	return m_invnorm;
}


// Catmull-Clark splitting function
// The split and subdivide are separated like this in order to allow
// the animation between the previous subdivision level and the next
Int32 Mesh::CatmullSplit( const Mesh& m, const HEData* pHEin )
{
	Reset();
    
	m_culling = m.m_culling;
	m_wireframe = m.m_wireframe;
	m_invnorm = m.m_invnorm;
	m_moved = m.m_moved;
	m_scaled = m.m_scaled;
    m_standardBorders = m.m_standardBorders;
    
    // Build HE data if the passed in data structure isn't set
	HEData pAllocHE;
	if( !pHEin )
	{
        exit(-22);
		if( Failed( pAllocHE.Build( m ) ) )
			return _ERR;
	}
	const HEData& he = pHEin ? *pHEin : pAllocHE;
    
    // Reserve enough room in the position and quad vectors
    // Note that only quads are produced from Catmull Subdivision
	m_pos.reserve( m.pos().size() + m.tri().size()/3 + m.quad().size()/4 + ( he.size() + he.borders() )/2 );
	m_quad.reserve( m.tri().size()*4 + m.quad().size()*4 );
    
	// FACE VERTICES
    // Simply get the centroids of each triangle face
	for( Int32 t = 0; t < m.tri().size()/3; t++ )
	{
		Int32 i = t * 3;
		Vector3 avg = m.pos()[m.tri()[i].v].pos + m.pos()[m.tri()[i+1].v].pos + m.pos()[m.tri()[i+2].v].pos;
		avg /= 3;
        VertexInfo v;
        v.pos = avg;
        v.hard = false;
		m_pos.push_back( v );
	}
    // Simply get the centroids of each quad face
	for( Int32 q = 0; q < m.quad().size()/4; q++ )
	{
		Int32 i = q * 4;
		Vector3 avg = m.pos()[m.quad()[i].v].pos
                        + m.pos()[m.quad()[i+1].v].pos
                        + m.pos()[m.quad()[i+2].v].pos
                        + m.pos()[m.quad()[i+3].v].pos;
		avg /= 4;
        VertexInfo v;
        v.pos = avg;
        v.hard = false;
        
		m_pos.push_back( v );
	}
    
	// EDGE VERTICES
    // before repositionining in subdiv function, just keep at center
    // of edge between the two vertices
	std::vector<Int32> he_v;
	he_v.reserve( he.size() );
	he_v.resize( he.size() );
	for( Int32 i = 0; i < he.size(); i++ )
	{
        // If this isn't already set...
		if( he[i].pair < i )
		{
			he_v[i] = m_pos.size();
			if( he[i].pair >= 0 )
				he_v[he[i].pair] = m_pos.size();
            
			Vector3 avg = m.pos()[he[i].v].pos + m.pos()[he[he[i].next].v].pos;
			avg /= 2;
            VertexInfo v;
            v.pos = avg;
            v.hard = m.pos()[he[i].v].hard && m.pos()[he[he[i].next].v].hard;
			m_pos.push_back( v );
		}
	}
    
	// VERTEX VERTICES
	for( Int32 i = 0; i < m.pos().size(); i++ )
		m_pos.push_back( m.pos()[i] );
    
	// REBUILD TOPOLOGY
    
	Int32 ff = 0;
	Int32 ef = ff + m.tri().size()/3 + m.quad().size()/4;
	Int32 vf = ef + ( he.size() + he.borders() )/2;
    
	for( Int32 t = 0; t < m.tri().size()/3; t++ )
	{
		Int32 i = t * 3;
        
        // Set the vertices up, with half-edge vertices that have been added as the edge vertices
		// and the vertex vertices as the existing vertices in the model
		Int32 f1 = t;
		Int32 e1 = he_v[i];
		Int32 e2 = he_v[i+1];
		Int32 e3 = he_v[i+2];
		Int32 v1 = m.tri()[i  ].v + vf;
		Int32 v2 = m.tri()[i+1].v + vf;
		Int32 v3 = m.tri()[i+2].v + vf;

        // counter-clockwise definitions, going v1->e1->f1->e3
		m_quad.push_back( Index( e1, m.tri()[i  ].c  ) );
		m_quad.push_back( Index( f1, m.tri()[i  ].c  ) );
		m_quad.push_back( Index( e3, m.tri()[i  ].c  ) );
		m_quad.push_back( Index( v1, m.tri()[i  ].c  ) );
        
        // counter-clockwise definitions, going v2->e2->f1->e1
		m_quad.push_back( Index( e2, m.tri()[i  ].c  ) );
		m_quad.push_back( Index( f1, m.tri()[i  ].c  ) );
		m_quad.push_back( Index( e1, m.tri()[i  ].c  ) );
		m_quad.push_back( Index( v2, m.tri()[i  ].c  ) );
        
        // counter-clockwise definitions, going v3->e3->f1->e2
		m_quad.push_back( Index( e3, m.tri()[i  ].c  ) );
		m_quad.push_back( Index( f1, m.tri()[i  ].c  ) );
		m_quad.push_back( Index( e2, m.tri()[i  ].c  ) );
		m_quad.push_back( Index( v3, m.tri()[i  ].c  ) );
	}
    
    // Need to offset into the he vertices and the quad vector
	Int32 he_v_ofs = m.tri().size();
	Int32 ff_v_ofs = he_v_ofs / 3;
	for( Int32 q = 0; q < m.quad().size()/4; q++ )
	{
		Int32 i = q * 4;
  
        // Set the vertices up, with half-edge vertices that have been added as the edge vertices
		// and the vertex vertices as the existing vertices in the model
		Int32 f1 = ff_v_ofs + q;
		Int32 e1 = he_v[he_v_ofs+i];
		Int32 e2 = he_v[he_v_ofs+i+1];
		Int32 e3 = he_v[he_v_ofs+i+2];
		Int32 e4 = he_v[he_v_ofs+i+3];
		Int32 v1 = m.quad()[i  ].v + vf;
		Int32 v2 = m.quad()[i+1].v + vf;
		Int32 v3 = m.quad()[i+2].v + vf;
		Int32 v4 = m.quad()[i+3].v + vf;

        // counter-clockwise definitions, going v4->e4->f1->e3
		m_quad.push_back( Index( e4, m.quad()[i  ].c ) );
		m_quad.push_back( Index( f1, m.quad()[i  ].c ) );
		m_quad.push_back( Index( e3, m.quad()[i  ].c ) );
		m_quad.push_back( Index( v4, m.quad()[i  ].c ) );
        
        // counter-clockwise definitions, going v2->e2->f1->e1
		m_quad.push_back( Index( e2, m.quad()[i  ].c ) );
		m_quad.push_back( Index( f1, m.quad()[i  ].c ) );
		m_quad.push_back( Index( e1, m.quad()[i  ].c ) );
		m_quad.push_back( Index( v2, m.quad()[i  ].c ) );

        // counter-clockwise definitions, going v1->e1->f1->e4
		m_quad.push_back( Index( e1, m.quad()[i  ].c ) );
		m_quad.push_back( Index( f1, m.quad()[i  ].c ) );
		m_quad.push_back( Index( e4, m.quad()[i  ].c ) );
		m_quad.push_back( Index( v1, m.quad()[i  ].c ) );
        
        // counter-clockwise definitions, going v3->e3->f1->e2
		m_quad.push_back( Index( e3, m.quad()[i  ].c ) );
		m_quad.push_back( Index( f1, m.quad()[i  ].c ) );
		m_quad.push_back( Index( e2, m.quad()[i  ].c ) );
		m_quad.push_back( Index( v3, m.quad()[i  ].c ) );
	}
    
	_CalcNormals();
    
	return _OK;
}

// Application of catmull-clark masks
// The split and subdivide are separated like this in order to allow
// the animation between the previous subdivision level and the next
Int32 Mesh::CatmullSubdivision( const Mesh& m, const HEData* pHEin )
{
	HEData pAllocHE;
	if( !pHEin )
	{
		if( Failed( pAllocHE.Build( m ) ) )
			return _ERR;
	}
	const HEData& he = pHEin ? *pHEin : pAllocHE;
    
	if( Failed( CatmullSplit( m, &he ) ) )
		return _ERR;
    
    // Calculate offsets
	Int32 v = m.tri().size()/3 + m.quad().size()/4;
	Int32 ofs = v + ( he.size() + he.borders() )/2;
    
	std::vector<bool> done;
	done.reserve( m.pos().size() );
	done.resize( m.pos().size() );
	for( Int32 i = 0; i < he.size(); i++ )
	{
		// EDGE VERTICES
        // If this hasn't already been calculated
		if( he[i].pair < i )
		{
            // If not a border case
			if( he[i].pair >= 0 )
			{
				Int32 f1 = he[i].f;
				Int32 f2 = he[he[i].pair].f;
                
                // filter is 1/4(f1 + f2 + v1 + v2), where v1
                // and v2 are the edge's vertices. Note that
                // the split already calculated this
                // in order to facilitate animation so
                // m_pos[v] is already 1/2(v1 + v2)
				Vector3 avg = m_pos[f1].pos + m_pos[f2].pos;
				avg /= 2;
                
				m_pos[v].pos = ( m_pos[v].pos + avg ) / 2;
			}
			v++;
		}
        
		// VERTEX VERTICES
		if( !done[he[i].v] )
		{
			Vector3 edges = Vector3( 0, 0, 0 );
			Vector3 faces = Vector3( 0, 0, 0 );
			Int32 n = 0;
			Int32 he_f = he[i].pair;
			Int32 he_n = he_f;
			Int32 he_o = i;
			while( he_n >= 0 )
			{
				n++;
				edges += m.pos()[he[he_n].v].pos;
				faces += m_pos[he[he_n].f].pos;
                
				he_o = he[he_n].next;
				he_n = he[he_o].pair;
				if( he_n == he_f )
					break;
			}
            // If not a border case, then the filter is
            // (n-2/n) * current vertex + 1/(n*n) *  new edges
            // + 1/(n*n) * new faces
			if( he_n >= 0 )
			{
				Real rp1 = ( (Real)( n - 2 ) / n );
				Real rp23 = ( 1.0f / ( n * n ) );
				Vector3 p1 = rp1 * m_pos[ofs+he[i].v].pos;
				Vector3 p2 = rp23 * edges;
				Vector3 p3 = rp23 * faces;
				
				m_pos[ofs+he[i].v].pos = p1 + p2 + p3;
			}
            //if standard borders, then do cubic b-spline subdivision filters
            // along border
            else if( m_standardBorders )
            {
                Int32 he_l;
                Int32 he_p = i;
                do
                {
                    he_f = he_p;
                    he_p = he[he_p].next;
                    while( he[he_p].next != he_f )
                        he_p = he[he_p].next;

                    he_l = he_p;
                    he_p = he[he_p].pair;
                } while( he_p >= 0 );

                const Vector3& p1 = m.pos()[he[i].v].pos;
                const Vector3& p2 = m.pos()[he[he[he_o].next].v].pos;
                const Vector3& p3 = m.pos()[he[he_l].v].pos;

                m_pos[ofs+he[i].v].pos = 0.75f * p1 + 0.125f * p2 + 0.125 * p3;
            }
			done[he[i].v] = true;
		}
	}
	return _OK;
}

// Simple interpolation between 2 meshes - used for animation
Int32 Mesh::Interpolate( const Mesh& mFrom, const Mesh& mTo, float animationValue )
{
    this->Copy(mFrom); // Sets sizings properly
    
    if( animationValue <= 1.001f && animationValue >= 0.0f )
    {
        for( int i = 0; i < m_pos.size(); ++i )
        {
            m_pos[i].pos = (mTo.m_pos[i].pos * animationValue) + (mFrom.m_pos[i].pos * (1 - animationValue));
            m_norm[i] = (mTo.m_norm[i] * animationValue) + (mFrom.m_norm[i] * (1 - animationValue));
        }
    }
    return _OK;
}

// Simple copy function
Int32 Mesh::Copy( const Mesh& m )
{
    m_culling = m.m_culling;
    m_wireframe = m.m_wireframe;
    m_invnorm = m.m_invnorm;
    m_standardBorders = m.m_standardBorders;
    
    m_pos = m.m_pos;
    m_tex = m.m_tex;
    m_norm = m.m_norm;

    m_tri = m.m_tri;
    m_quad = m.m_quad;
    
    m_min = m.m_min;
    m_max = m.m_max;
    
    m_moved = m.m_moved;
    m_scaled = m.m_scaled;
    
    return _OK;
}

// Apply laplacian to the mesh
Int32 Mesh::Laplacian( const Mesh& m, const HEData* pHEin )
{
    //Build half-edge
    HEData pAllocHE;
    if( !pHEin )
    {
        if( Failed( pAllocHE.Build( m ) ) )
            return _ERR;
    }
    const HEData& he = pHEin ? *pHEin : pAllocHE;
 
    Copy( m );
 
    std::vector<bool> done;
    done.reserve( m.pos().size() );
    done.resize( m.pos().size() );
 
    for( Int32 i = 0; i < he.size(); i++ )
    {
        if( !done[he[i].v] )
        {
             //Gather all neighboring vertices and sum them up
            Vector3 v = Vector3( 0, 0, 0 );
            Int32 he_f = he[i].next;
            Int32 he_n = he_f;
            Int32 nCount = 0;
            do
            {
                v += m.pos()[he[he_n].v].pos;
                nCount++;
                
                while( he[he[he_n].next].v != he[i].v )
                    he_n = he[he_n].next;
                
                he_n = he[he_n].pair;
                if( he_n < 0 )
                {
                    v = m.pos()[he[i].v].pos;
                    nCount = 1;
                    break;
                }
                he_n = he[he_n].next;
            } while( he_n != he_f ); // while not at start...
            
             //set vertex to the average of the neighbors
            m_pos[he[i].v].pos = v / nCount;
            done[he[i].v] = true;
        }
    }
    
    return _OK;
}

// Perform loop split phase.
// The split and subdivide are separated like this in order to allow
// the animation between the previous subdivision level and the next
Int32 Mesh::LoopSplit( const Mesh& m, const HEData* pHEin )
{
    // Ensure mesh is triangular
	if( m.quad().size() > 0 )
		return _ERR;
    
    // Ensure HE is built
	HEData pAllocHE;
	if( !pHEin )
	{
		if( Failed( pAllocHE.Build( m ) ) )
			return _ERR;
	}
	const HEData& he = pHEin ? *pHEin : pAllocHE;
    
	Reset();

    // We are building the subdivided mesh from an input mesh,
    // so reset the mesh and set all params appropriately
	m_culling = m.m_culling;
	m_wireframe = m.m_wireframe;
	m_invnorm = m.m_invnorm;
	m_moved = m.m_moved;
	m_scaled = m.m_scaled;
    m_standardBorders = m.m_standardBorders;
    
	// EDGE VERTICES
	std::vector<Int32> he_v;    	// Half-edge index vector
	he_v.reserve( he.size() );
	he_v.resize( he.size() );
	for( Int32 i = 0; i < he.size(); i++ )
	{
        // If the edge hasn't already been handled...
		if( he[i].pair < i )
		{
			he_v[i] = m_pos.size();
            // if not a border case, set index to 1 beyond the m_pos
            // end, since we will be adding that vertex below
			if( he[i].pair >= 0 )
				he_v[he[i].pair] = m_pos.size();
            
            // Simply set as midway point of edge
			Vector3 avg = m.pos()[he[i].v].pos + m.pos()[he[he[i].next].v].pos;
			avg /= 2;
            VertexInfo v;
            v.pos = avg;
            v.hard = m.pos()[he[i].v].hard;
			m_pos.push_back( v );
		}
	}
    
	// VERTEX VERTICES
    // Pass through vertex-vertices
	for( Int32 i = 0; i < m.pos().size(); i++ )
		m_pos.push_back( m.pos()[i] );
    
	// REBUILD TOPOLOGY
	Int32 vf = ( he.size() + he.borders() )/2;
	for( Int32 t = 0; t < m.tri().size()/3; t++ )
	{
		Int32 i = t * 3;
        
		Int32 e1 = he_v[i];
		Int32 e2 = he_v[i+1];
		Int32 e3 = he_v[i+2];
		Int32 v1 = m.tri()[i  ].v + vf;
		Int32 v2 = m.tri()[i+1].v + vf;
		Int32 v3 = m.tri()[i+2].v + vf;
        
		// Define the faces counterclockwise, with E1 being the counterclockwise
        // edge of v1 etc. Add these faces to triangles list
		m_tri.push_back( Index( v1, m.tri()[i  ].c ) );
		m_tri.push_back( Index( e1, m.tri()[i  ].c ) );
		m_tri.push_back( Index( e3, m.tri()[i  ].c ) );
        
		m_tri.push_back( Index( v2, m.tri()[i  ].c ) );
		m_tri.push_back( Index( e2, m.tri()[i  ].c ) );
		m_tri.push_back( Index( e1, m.tri()[i  ].c ) );
        
		m_tri.push_back( Index( v3, m.tri()[i  ].c ) );
		m_tri.push_back( Index( e3, m.tri()[i  ].c ) );
		m_tri.push_back( Index( e2, m.tri()[i  ].c ) );
        
		m_tri.push_back( Index( e1, m.tri()[i  ].c ) );
		m_tri.push_back( Index( e2, m.tri()[i  ].c ) );
		m_tri.push_back( Index( e3, m.tri()[i  ].c ) );
	}
	
	_CalcNormals();
    
	return _OK;
}

// Loop subdivision method
Int32 Mesh::LoopSubdiv( const Mesh& m, const HEData* pHEin )
{
	if( m.quad().size() > 0 )
		return _ERR;
    
	HEData pAllocHE;
	if( !pHEin )
	{
		if( Failed( pAllocHE.Build( m ) ) )
			return _ERR;
	}
	const HEData& he = pHEin ? *pHEin : pAllocHE;
    
	if( Failed( LoopSplit( m, &he ) ) )
		return _ERR;
    
    // For keeping track of which vertices have been repositioned
	std::vector<bool> done;
	done.reserve( m.pos().size() );
	done.resize( m.pos().size() );
	Int32 vf = ( he.size() + he.borders() )/2;
	Int32 v = 0;
	for( Int32 i = 0; i < he.size(); i++ )
	{
		// EDGE VERTICES
        // If this vertex hasn't already been handled...
		if( he[i].pair < i )
		{
            // and if not a border case, then reposition
			if( he[i].pair >= 0 )
			{
				Int32 n = he[i].next;
				Int32 nn = he[n].next;
				Int32 pnn = he[he[he[i].pair].next].next;
				Vector3 avg1 = m.pos()[he[i].v].pos + m.pos()[he[n].v].pos;
				Vector3 avg2 = m.pos()[he[nn].v].pos + m.pos()[he[pnn].v].pos;
				avg1 *= (3.0f/8.0f);
				avg2 *= (1.0f/8.0f);
				m_pos[v].pos = avg1 + avg2;
			}
			v++;
		}
        
		// VERTEX VERTICES
		if( !done[he[i].v] )
		{
			Vector3 total = Vector3( 0, 0, 0 );
			Int32 n = 0;
			Int32 he_f = he[i].pair;
			Int32 he_n = he_f;
			Int32 he_o = i;
            
            // While no borders have been hit, sum up the
            // vertices around the 'perimeter'
			while( he_n >= 0 )
			{
				n++;
				total += m.pos()[he[he_n].v].pos;
                
				he_o = he[he_n].next;
				he_n = he[he_o].pair;
				if( he_n == he_f )
					break;
			}
            // If not a border case, apply regular filters
			if( he_n >= 0 )
			{
                Real cosn = COS( ( 2 * PI ) / n );
                Real internal = (3.0f/8.0f) + (1.0f/4.0f)*cosn;
                Real a = (1.0f/n) * ( (5.0f/8.0f) - ( internal * internal ) );
                m_pos[vf+he[i].v].pos = ( 1.0f - n * a ) * m.pos()[he[i].v].pos  + a * total;
			}
            // else apply cubic b-spline filter along the border
			else if( m_standardBorders )
			{
                Int32 he_l;
                Int32 he_p = i;
                do
                {
                    he_f = he_p;
                    he_p = he[he_p].next;
                    while( he[he_p].next != he_f )
                        he_p = he[he_p].next;
                    
                    he_l = he_p;
                    he_p = he[he_p].pair;
                } while( he_p >= 0 );
                
                const Vector3& p1 = m.pos()[he[i].v].pos;
                const Vector3& p2 = m.pos()[he[he[he_o].next].v].pos;
                const Vector3& p3 = m.pos()[he[he_l].v].pos;
                
                m_pos[vf+he[i].v].pos = 0.75f * p1 + 0.125f * p2 + 0.125 * p3;
			}
			done[he[i].v] = true;
		}
	}
    
	return _OK;
}

Int32 Mesh::Triangulate( const Mesh& m )
{
	if( Failed( Copy( m ) ) )
		return _ERR;
    
    Int32 c = ( m_tri.size() / 3 ) + 1;
	for( Int32 i = 0; i < m_quad.size()/4; i++ )
	{
		Int32 q = i * 4;
		m_tri.push_back( m_quad[q] );
        m_tri[m_tri.size()-1].c = c;
		m_tri.push_back( m_quad[q+1] );
        m_tri[m_tri.size()-1].c = c;
		m_tri.push_back( m_quad[q+2] );
        m_tri[m_tri.size()-1].c = c++;
       
		m_tri.push_back( m_quad[q] );
        m_tri[m_tri.size()-1].c = c;
		m_tri.push_back( m_quad[q+2] );
        m_tri[m_tri.size()-1].c = c;
		m_tri.push_back( m_quad[q+3] );
        m_tri[m_tri.size()-1].c = c++;
	}
    
	m_quad.clear();
    
	return _OK;
}

Int32 Mesh::Randomize( Real fPercent, Real fForce )
{
	Int32 nCount = m_pos.size() * fPercent * (fForce / (m_pos.size() * .02)) + 1;
    
    
	for( Int32 i = 0; i < nCount; i++ )
	{
		Real fPower = (Real)( rand() % 1000 ) / 1000.0f;
		fPower *= fForce;
        
		Int32 nVert = rand() % m_pos.size();
		Int32 nNorm = rand() % m_norm.size();
        
		m_pos[nVert].pos += fPower * m_norm[nNorm];
	}
    
	return _OK;
}

Real ClosestSqSegmentSegment( Vector3 pts[2], const Vector3& s1a, const Vector3& s1b, const Vector3& s2a, const Vector3& s2b )
{
	Vector3 u = s1b - s1a;
	Vector3 v = s2b - s2a;
	Vector3 w = s1a - s2a;
	Real UdotU = Vector3::Dot( u, u );
	Real UdotV = Vector3::Dot( u, v );
	Real VdotV = Vector3::Dot( v, v );
	Real UdotW = Vector3::Dot( u, w );
	Real VdotW = Vector3::Dot( v, w );
	Real D = UdotU * VdotV - UdotV * UdotV;
	Real sc, sN, sD = D;
	Real tc, tN, tD = D;
    
	if( D < CLOSE )
	{
		sN = 0;
		sD = 1;
		tN = VdotW;
		tD = VdotV;
	}
	else
	{
		sN = UdotV * VdotW - VdotV * UdotW;
		tN = UdotU * VdotW - UdotV * UdotW;
		if( sN < 0 )
		{
			sN = 0;
			tN = VdotW;
			tD = VdotV;
		}
		else if( sN > sD )
		{
			sN = sD;
			tN = VdotW + UdotV;
			tD = VdotV;
		}
	}
    
	if( tN < 0 )
	{
		tN = 0;
		if( -UdotW < 0 )
			sN = 0;
		else if( -UdotW > UdotU )
			sN = sD;
		else
		{
			sN = -UdotW;
			sD = UdotU;
		}
	}
	else if( tN > tD )
	{
		tN = tD;
		if( -UdotW + UdotV < 0 )
			sN = 0;
		else if( -UdotW + UdotV > UdotU )
			sN = sD;
		else
		{
			sN = -UdotW + UdotV;
			sD = UdotU;
		}
	}
    
	sc = ABS( sN ) < CLOSE ? 0 : sN / sD;
	tc = ABS( tN ) < CLOSE ? 0 : tN / tD;
    
	Vector3 uSC = u * sc;
	Vector3 vTC = v * tc;
    
	pts[0] = s1a + uSC;
	pts[1] = s2a + vTC;
    
	Vector3 dP = w + uSC - vTC;
	return dP.SquaredLength();
}


// Broken edge picking. Won't pick correctly for all edges since I used faces for picking
// No time to fix this
bool Mesh::IntersectionOccurs( Vector3 rayOrigin, Vector3 rayEnd, const HEData& he )
{
    float closestDistance = 100.0f;
    Vector3 intPoints[2];
    int indexA = -1;
    int indexB = -1;
    bool resultInTris = true;
    

    
    for( int i = 0; i < m_tri.size(); i += 3 )
    {
        for( int j = 0; j < 3; ++j )
        {
            Vector3 segmentA = m_pos[m_tri[ i + j ].v].pos;
            Vector3 segmentB = m_pos[m_tri[ i + ( (j + 1) % 3 ) ].v].pos;
            
            Real fDistance = ClosestSqSegmentSegment(intPoints, rayOrigin, rayEnd, segmentA, segmentB);
            if( fDistance < 0.001 && fDistance < closestDistance  )
            {
                closestDistance = fDistance;
                indexA = i + j;
                indexB = i + ( (j + 1) % 3 );
            }
        }
    }
    
    for( int i = 0; i < m_quad.size(); i += 4 )
    {
        for( int j = 0; j < 4; ++j )
        {
            Vector3 segmentA = m_pos[m_quad[ i + j].v].pos;
            Vector3 segmentB = m_pos[m_quad[ i + ( (j + 1) % 4) ].v].pos;
            
            Real fDistance = ClosestSqSegmentSegment(intPoints, rayOrigin, rayEnd, segmentA, segmentB);
            if( fDistance < 0.001 && fDistance < closestDistance )
            {
                closestDistance = fDistance;
                
                indexA = i + j;
                indexB = i + ( (j + 1) % 4 );
                resultInTris = false;
            }
        }
    }

    if( closestDistance < 1 )
    {
        Vector3 closeA, closeB;
        closeA = resultInTris ? m_pos[m_tri[ indexA ].v].pos : m_pos[m_quad[ indexA ].v].pos;
        closeB = resultInTris ? m_pos[m_tri[ indexB ].v].pos : m_pos[m_quad[ indexB ].v].pos;
        
        NSLog(@"A: %.02f, %.02f, %.02f", closeA.x, closeA.y, closeA.z );
        NSLog(@"B: %.02f, %.02f, %.02f", closeB.x, closeB.y, closeB.z );
        
        if( resultInTris )
        {
            m_pos[m_tri[ indexA ].v].hard = m_pos[m_tri[ indexA ].v].hard ? false : true;;
        }
        else
        {
            m_pos[m_quad[ indexA ].v].hard = m_pos[m_quad[ indexA ].v].hard ? false : true;
        }
        
        return true;
    }

    return false;
}

