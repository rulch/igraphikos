//
//  GLMesh.cpp
//  iGraphikos
//
//  Created by Ryan E. Ulch on 11/20/13.
//  Copyright (c) 2013 Ryan E. Ulch. All rights reserved.
//

#include "GLMesh.h"
#include "Colors.h"
#define BUFFER_OFFSET(i) ((char *)NULL + (i))

Vector4 hardEdgeColor;

GLMesh::GLMesh()
{
    m_bLines = 0;
    m_glVBuffer = 0;
    m_nVertexCount = 0;
    hardEdgeColor.x = 1.0f;
    hardEdgeColor.y = 1.0f;
    hardEdgeColor.z = 0.0f;
}

GLMesh::~GLMesh()
{
    glDeleteVertexArraysOES( 1, &m_glVBuffer );
}

int GLMesh::FromMesh( const Mesh& m, int bLines, Vector4 vColor, BOOL bColor )
{
    if( bLines )
        bColor = NO;
    
    m_bLines = bLines;
    if( m.pos().size() != m_nVertexCount )
    {
        if( bLines )
            m_nVertexCount = m.tri().size() * 2 + m.quad().size() * 2;
        else
            m_nVertexCount = m.tri().size() + m.quad().size() + ( m.quad().size() >> 1 );
        
        if( !m_glVBuffer )
            glGenVertexArraysOES( 1, &m_glVBuffer );
        
        // SET BUFFER SIZE
        glBindBuffer( GL_ARRAY_BUFFER, m_glVBuffer );
        glBindVertexArrayOES( m_glVBuffer );
        
        glBufferData( GL_ARRAY_BUFFER, sizeof(Vertex) * m_nVertexCount, 0, GL_DYNAMIC_DRAW );
        
   }
    
    // UPDATE VERTICES map/unmap
    
    // UPDATE TRIANGLES map/unmap
    glBindBuffer( GL_ARRAY_BUFFER, m_glVBuffer );
    glBindVertexArrayOES( m_glVBuffer );
    Vertex* pData = (Vertex*)glMapBufferOES( GL_ARRAY_BUFFER, GL_WRITE_ONLY_OES );
    
    int v = 0;
    
    if( !bLines )
    {
        for( int i = 0; i < m.tri().size(); i++ )
        {
            pData[v].pos = m.pos()[m.tri()[i].v].pos;
            pData[v].norm = m.norm()[m.tri()[i].n];
            pData[v].color = bColor ? GetUniqueColor4f( m.tri()[i].c ) : vColor;
            v++;
        }
        
        for( int q = 0; q < m.quad().size()/4; q++ )
        {
            int i = q * 4;
            pData[v].pos = m.pos()[m.quad()[i].v].pos;
            pData[v].norm = m.norm()[m.quad()[i].n];
            pData[v].color = bColor ? GetUniqueColor4f( m.quad()[i].c ) : vColor;
            v++;
            pData[v].pos = m.pos()[m.quad()[i+1].v].pos;
            pData[v].norm = m.norm()[m.quad()[i+1].n];
            pData[v].color = bColor ? GetUniqueColor4f( m.quad()[i].c ) : vColor;
            v++;
            pData[v].pos = m.pos()[m.quad()[i+2].v].pos;
            pData[v].norm = m.norm()[m.quad()[i+2].n];
            pData[v].color = bColor ? GetUniqueColor4f( m.quad()[i].c ) : vColor;
            v++;
            
            pData[v].pos = m.pos()[m.quad()[i+2].v].pos;
            pData[v].norm = m.norm()[m.quad()[i+2].n];
            pData[v].color = bColor ? GetUniqueColor4f( m.quad()[i].c ) : vColor;
            v++;
            pData[v].pos = m.pos()[m.quad()[i+3].v].pos;
            pData[v].norm = m.norm()[m.quad()[i+3].n];
            pData[v].color = bColor ? GetUniqueColor4f( m.quad()[i].c ) : vColor;
            v++;
            pData[v].pos = m.pos()[m.quad()[i].v].pos;
            pData[v].norm = m.norm()[m.quad()[i].n];
            pData[v].color = bColor ? GetUniqueColor4f( m.quad()[i].c ) : vColor;
            v++;
        }
    }
    else
    {
        for( int t = 0; t < m.tri().size()/3; t++ )
        {
            int i = t * 3;
            
            pData[v].pos = m.pos()[m.tri()[i].v].pos;
            pData[v].norm = m.norm()[m.tri()[i].n];
            pData[v].color = bColor ? GetUniqueColor4f( m.tri()[i].c ) : m.pos()[m.tri()[i].v].hard? hardEdgeColor : vColor;
            v++;
            
            pData[v].pos = m.pos()[m.tri()[i+1].v].pos;
            pData[v].norm = m.norm()[m.tri()[i+1].n];
            pData[v].color = bColor ? GetUniqueColor4f( m.tri()[i].c ) : m.pos()[m.tri()[i+1].v].hard? hardEdgeColor : vColor;
            v++;
            
            pData[v].pos = m.pos()[m.tri()[i+1].v].pos;
            pData[v].norm = m.norm()[m.tri()[i+1].n];
            pData[v].color = bColor ? GetUniqueColor4f( m.tri()[i].c ) : m.pos()[m.tri()[i+1].v].hard? hardEdgeColor : vColor;
            v++;
            
            pData[v].pos = m.pos()[m.tri()[i+2].v].pos;
            pData[v].norm = m.norm()[m.tri()[i+2].n];
            pData[v].color = bColor ? GetUniqueColor4f( m.tri()[i].c ) : m.pos()[m.tri()[i+2].v].hard? hardEdgeColor : vColor;
            v++;
            
            pData[v].pos = m.pos()[m.tri()[i+2].v].pos;
            pData[v].norm = m.norm()[m.tri()[i+2].n];
            pData[v].color = bColor ? GetUniqueColor4f( m.tri()[i].c ) : m.pos()[m.tri()[i+2].v].hard? hardEdgeColor : vColor;
            v++;
            
            pData[v].pos = m.pos()[m.tri()[i].v].pos;
            pData[v].norm = m.norm()[m.tri()[i].n];
            pData[v].color = bColor ? GetUniqueColor4f( m.tri()[i].c ) : m.pos()[m.tri()[i].v].hard? hardEdgeColor : vColor;
            v++;
        }
        
        for( int q = 0; q < m.quad().size()/4; q++ )
        {
            int i = q * 4;
            
            pData[v].pos = m.pos()[m.quad()[i].v].pos;
            pData[v].norm = m.norm()[m.quad()[i].n];
            pData[v].color = bColor ? GetUniqueColor4f( m.quad()[i].c ) : m.pos()[m.quad()[i].v].hard? hardEdgeColor : vColor;
            v++;
            
            pData[v].pos = m.pos()[m.quad()[i+1].v].pos;
            pData[v].norm = m.norm()[m.quad()[i+1].n];
            pData[v].color = bColor ? GetUniqueColor4f( m.quad()[i].c ) : m.pos()[m.quad()[i+1].v].hard? hardEdgeColor : vColor;
            v++;
            
            pData[v].pos = m.pos()[m.quad()[i+1].v].pos;
            pData[v].norm = m.norm()[m.quad()[i+1].n];
            pData[v].color = bColor ? GetUniqueColor4f( m.quad()[i].c ) : m.pos()[m.quad()[i+1].v].hard? hardEdgeColor : vColor;
            v++;
            
            pData[v].pos = m.pos()[m.quad()[i+2].v].pos;
            pData[v].norm = m.norm()[m.quad()[i+2].n];
            pData[v].color = bColor ? GetUniqueColor4f( m.quad()[i].c ) : m.pos()[m.quad()[i+2].v].hard? hardEdgeColor : vColor;
            v++;
            
            pData[v].pos = m.pos()[m.quad()[i+2].v].pos;
            pData[v].norm = m.norm()[m.quad()[i+2].n];
            pData[v].color = bColor ? GetUniqueColor4f( m.quad()[i].c ) : m.pos()[m.quad()[i+2].v].hard? hardEdgeColor : vColor;
            v++;
            
            pData[v].pos = m.pos()[m.quad()[i+3].v].pos;
            pData[v].norm = m.norm()[m.quad()[i+3].n];
            pData[v].color = bColor ? GetUniqueColor4f( m.quad()[i].c ) : m.pos()[m.quad()[i+3].v].hard? hardEdgeColor : vColor;
            v++;
            
            pData[v].pos = m.pos()[m.quad()[i+3].v].pos;
            pData[v].norm = m.norm()[m.quad()[i+3].n];
            pData[v].color = bColor ? GetUniqueColor4f( m.quad()[i].c ) : m.pos()[m.quad()[i+3].v].hard? hardEdgeColor : vColor;
            v++;
            
            pData[v].pos = m.pos()[m.quad()[i].v].pos;
            pData[v].norm = m.norm()[m.quad()[i].n];
            pData[v].color = bColor ? GetUniqueColor4f( m.quad()[i].c ) : m.pos()[m.quad()[i].v].hard? hardEdgeColor : vColor;
            v++;
        }
    }
    
    glUnmapBufferOES( GL_ARRAY_BUFFER );
    glBindVertexArrayOES( 0 );
   
    return 0;
}

int GLMesh::Prepare()
{
    glBindBuffer( GL_ARRAY_BUFFER, m_glVBuffer );
    glBindVertexArrayOES( m_glVBuffer );
    
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 40, BUFFER_OFFSET(0));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 40, BUFFER_OFFSET(12));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, 40, BUFFER_OFFSET(24));
   
    return 0;
}

int GLMesh::Draw()
{
    glLineWidth( 2.5 );
    glDrawArrays( m_bLines ? GL_LINES : GL_TRIANGLES, 0, m_nVertexCount );
    return 0;
}








































