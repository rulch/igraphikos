//
//  InformationViewController.h
//  iGraphikos
//
//  Created by Ryan E. Ulch on 12/3/13.
//  Copyright (c) 2013 Ryan E. Ulch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewControllerCommunicationDelegate.h" // For demo enums


@interface InformationViewController : UIViewController
@property (nonatomic, assign) DemoEnum_t demoType;

@property (nonatomic, weak) IBOutlet InformationViewController* mHalfEdgeInfo;

// IBOutlets
@property (strong, nonatomic) IBOutlet UITextView *mGeneralOverviewText;
@property (strong, nonatomic) IBOutlet UITextView *mSecondaryText;

// Actions
- (IBAction)CloseView:(UIButton *)sender;

- (IBAction)GeneralLink1:(UIButton *)sender;
- (IBAction)GeneralLink2:(UIButton *)sender;
- (IBAction)GeneralLink3:(UIButton *)sender;

- (IBAction)SecondaryLink1:(UIButton *)sender;
- (IBAction)SecondaryLink2:(UIButton *)sender;

- (IBAction)TriggerHalfEdgeInfo:(UIButton *)sender;



@end
